module.exports = {
  projects: [
    '<rootDir>/apps/phenom-communications-backend',
    '<rootDir>/libs/phenom-communications-ui',
    '<rootDir>/apps/lib-development',
    '<rootDir>/apps/k8s-infrastructure',
    '<rootDir>/apps/email-worker',
    '<rootDir>/libs/phenom-communications-types',
    '<rootDir>/libs/phenom-mail-template',
    '<rootDir>/libs/phenom-communications-storybook',
  ],
};
