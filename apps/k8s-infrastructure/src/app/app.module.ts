import { Module } from '@nestjs/common';

import { K8sInfrastructureService } from './k8s-infrastructure.service';

@Module({
  imports: [],
  providers: [K8sInfrastructureService],
})
export class AppModule {}
