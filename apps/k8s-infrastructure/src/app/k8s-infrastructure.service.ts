import { Injectable } from '@nestjs/common';
import { Cdk8sApp as App } from '@hindawi/phenom-charts';
import { createK8sChart, PhenomEnvType } from '../lib';

@Injectable()
export class K8sInfrastructureService {
  public async createEnvironment(app: App, environment: PhenomEnvType): Promise<void> {
    await createK8sChart(app, `phenom-communications`, environment);
  }
}
