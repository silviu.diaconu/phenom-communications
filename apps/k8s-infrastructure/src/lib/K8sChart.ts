import { Construct } from 'constructs';

import { HindawiServiceChart } from '@hindawi/phenom-charts';
import { PhenomEnvType } from './PhenomEnvType';
import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts/lib/hindawi-service/chart';
import { StackOutputsLoader } from './stack-outputs.loader';

export const labels = {
  owner: 'gaia',
};


function getIngress(envName){
  if (envName === PhenomEnvType.PROD_GSW) {
    return `phenom-communications.gsw-prod.phenom.pub`
  }
  if (envName === PhenomEnvType.PROD_HINDAWI) {
    return `phenom-communications.prod.phenom.pub`
  }
  return `phenom-communications.${envName}.phenom.pub`
}
function getStackName(envName) {
  if (envName === PhenomEnvType.PROD_GSW) {
    return 'gsw-prod-phenom-communications';
  }
  return envName + '-phenom-communications';
}

function getEnvResources(envName) {
  //prod
  if (envName === PhenomEnvType.PROD_GSW || envName === PhenomEnvType.PROD_HINDAWI) {
    return {
      requests: { memory: '1000Mi' },
      limits: { memory: '1000Mi' },
    };
  }
  //everything else
  return {
    requests: { memory: '400Mi' },
    limits: { memory: '400Mi' },
  };
}

export const createK8sChart = async (
  scope: Construct,
  appName: string,
  environment: PhenomEnvType,
) => {
  const backendName = `${appName}-${environment}`;
  const emailWorkerName = `${appName}-email-worker-${environment}`;

  const redisHost = await StackOutputsLoader.instance.getOutput(
    getStackName(environment),
    'REDISHOST',
  );
  const redisPort = await StackOutputsLoader.instance.getOutput(
    getStackName(environment),
    'REDISPORT',
  );

  //backend
  await HindawiServiceChart.withAwsSecrets(scope, backendName, {
    secretNames: [`${environment}/phenom-communications`],

    serviceProps: {
      image: {
        repository: '916437579680.dkr.ecr.eu-west-1.amazonaws.com/phenom-communications',
        tag: process.env['CI_COMMIT_SHA'] || 'latest',
        pullPolicy: 'Always',
      },
      labels,
      service: {
        port: 80,
      },
      containerPort: 3000,
      resources: getEnvResources(environment),
      ingressOptions: {
        rules: [{ host: getIngress(environment) }],
      },
      envVars: {
        REDIS_HOST: redisHost,
        REDIS_PORT: redisPort,
      },
    },
  } as WithAwsSecretsServiceProps);

  //email worker
  await HindawiServiceChart.withAwsSecrets(scope, emailWorkerName, {
    secretNames: [`${environment}/phenom-communications`],

    serviceProps: {
      image: {
        repository:
          '916437579680.dkr.ecr.eu-west-1.amazonaws.com/phenom-communications-email-worker',
        tag: process.env['CI_COMMIT_SHA'] || 'latest',
        pullPolicy: 'Always',
      },
      labels,
      resources: getEnvResources(environment),
      envVars: {
        REDIS_HOST: redisHost,
        REDIS_PORT: redisPort,
      },
    },
  } as WithAwsSecretsServiceProps);
};
