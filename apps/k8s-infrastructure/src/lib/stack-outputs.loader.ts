import * as AWS from 'aws-sdk';
import { DescribeStacksOutput } from 'aws-sdk/clients/cloudformation';

export class StackOutputsLoader {
  private region = 'eu-west-1';
  private client: AWS.CloudFormation;

  public static instance: StackOutputsLoader = new StackOutputsLoader();

  private constructor() {
    this.client = new AWS.CloudFormation({
      region: this.region,
    });
  }

  public async getOutput(stackName: string, key: string): Promise<string> {
    try {
      const stacksOutput: DescribeStacksOutput = await this.client
        .describeStacks({
          StackName: stackName,
        })
        .promise();

      const output = stacksOutput.Stacks?.[0].Outputs?.filter(
        (output) => output.OutputKey == key,
      ).map((output) => output.OutputValue)?.[0];

      return output!;
    } catch (e) {
      console.error(`Error loading output ${key} from stack ${stackName}`, e, 'StackOutputsLoader');
      throw e;
    }
  }
}
