export enum PhenomEnvType {
  QA = 'qa',
  PROD_HINDAWI = 'prod-hindawi',
  PROD_GSW = 'prod-gsw',
  DEMO_SALES = 'demo-sales',
}
