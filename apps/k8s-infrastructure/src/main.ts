import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';

import { Cdk8sApp as App } from '@hindawi/phenom-charts';
import { K8sInfrastructureService } from './app/k8s-infrastructure.service';
import { PhenomEnvType } from './lib';

const app = new App({
  outdir:
    process.env.phenom_communications_yaml_output_dir ||
    'dist/apps/k8s-infrastructure/dist',
});

async function bootstrap() {
  const nestJsApp = await NestFactory.create(AppModule, {
    logger: ['log', 'debug', 'error', 'verbose', 'warn'],
  });
  const envirtonmentOption = process.argv[2] as PhenomEnvType;

  const k8sInfrastructureService = nestJsApp.get(K8sInfrastructureService);

  await k8sInfrastructureService.createEnvironment(app, envirtonmentOption);

  await app.synth();

  await nestJsApp.close();
}

bootstrap().then(() => {
  Logger.log('Done')
  process.exit(0);
});

