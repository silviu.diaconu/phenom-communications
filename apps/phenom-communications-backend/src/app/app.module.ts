import { Module } from '@nestjs/common';
import { GraphqlServerModule } from '../infrastructure/graphql/graphql-server.module';
import { EmailsQueueModule } from '../infrastructure/queue/emails-queue-module';

import { format, transports } from 'winston';
import { LoggerModule } from '@hindawi/logger';
import SlackHook from 'winston-slack-webhook-transport';

const myTransports = () => {
  const result = [];
  if (process.env.SLACK_WEBHOOK_URL) {
    result.push(
      new SlackHook({
        formatter: (info) => {
          return {
            blocks: [
              {
                type: 'header',
                text: {
                  type: 'plain_text',
                  text: process.env.NODE_ENV + " " + process.env.SERVICE_NAME + ": " + info.level ,
                },
              },
              {
                type: 'section',
                text: {
                  type: 'plain_text',
                  text: info.message,
                },
              },
            ],
          };
        },
        webhookUrl: process.env.SLACK_WEBHOOK_URL,
        level: 'error',
      }),
    );
  }

  result.push(
    new transports.Console({
      format: format.combine(
        format.errors({ stack: true }),
        format.timestamp({ format: 'isoDateTime' }),
        format.json(),
        format.prettyPrint(),
        format.colorize({ all: true }),
      ),
    }),
  );
  return result;
};

@Module({
  imports: [
    GraphqlServerModule,
    EmailsQueueModule,
    LoggerModule.winston(
      {
        env: process.env.NODE_ENV,
        applicationName: process.env.service_name,
      },
      {
        // level: 'debug',
        level: process.env.LOG_LEVEL || 'debug',
        transports: myTransports(),
      },
    ),
  ],
})
export class AppModule {}
