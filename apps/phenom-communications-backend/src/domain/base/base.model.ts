import { AutoMap } from 'nestjsx-automapper';

export abstract class BaseModel<M> {
  constructor(props?: Partial<M>) {
    Object.assign(this, props);
  }
  @AutoMap()
  id: string;
  @AutoMap()
  createdAt: Date;
  @AutoMap()
  updatedAt: Date;
}
