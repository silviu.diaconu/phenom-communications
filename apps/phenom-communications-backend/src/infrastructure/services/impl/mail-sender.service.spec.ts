import { Test } from '@nestjs/testing';
import { TOKENS } from '../../../core';
import { MailSenderService } from './mail-sender.service';
import { Queues, TemplateName, WorkerEvents } from '@hindawi/phenom-communications-types';
import { mailInputMock } from '../../template-engine/email-template-tokens/mocks';
import { EmailOptions } from '@hindawi/emails';

const mockHtmlString = '<div id="mailHtml"></div>';

class MockQueue {
  add = jest.fn().mockResolvedValue(bullJob);
}

class MockBullJob {
  id = "122124";
  finished = jest.fn();
}
const bullJob = new MockBullJob();

class MockTemplateEngine {
  generateEmail = jest.fn().mockReturnValue(mockHtmlString);
}

describe('MailSenderService', () => {
  let mailSenderService: MailSenderService;
  let queue: MockQueue;
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        {
          provide: TOKENS.MAIL_SENDER_SERVICE,
          useClass: MailSenderService,
        },
        {
          provide: `BullQueue_${Queues.EMAILS_WORKER_QUEUE}`,
          useClass: MockQueue,
        },
        {
          provide: TOKENS.TEMPLATE_ENGINE_SERVICE,
          useClass: MockTemplateEngine,
        },
      ],
    }).compile();

    queue = await moduleRef.resolve<MockQueue>(`BullQueue_${Queues.EMAILS_WORKER_QUEUE}`);
    mailSenderService = await moduleRef.resolve<MailSenderService>(TOKENS.MAIL_SENDER_SERVICE);
  });

  it('Should send mail', async () => {
    const resp = await mailSenderService.sendMail(TemplateName.revisionRequested, mailInputMock);
    const mail: EmailOptions = {
      to: mailInputMock.to,
      from: mailInputMock.from,
      html: mockHtmlString,
      subject: `${mailInputMock.manuscript.manuscriptId}: Revision requested`,
    };

    expect(queue.add).toHaveBeenCalledWith(WorkerEvents.SEND_EMAIL, mail, {
      attempts: 3,
      backoff: { delay: 5 * 1000, type: 'exponential' },
      timeout: 1000 * 60 * 60 * 24,
    });

    expect(resp.jobId).toStrictEqual("122124")
  });
});
