import { EmailOptions } from '@hindawi/emails';
import { TemplateName, WorkerEvents, Queues } from '@hindawi/phenom-communications-types';
import { Inject, Injectable } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { TOKENS } from '../../../core';
import { MailInput } from '../../graphql/emailTemplate/emailTemplate.model';
import { TemplateEngineService } from '../../template-engine/template-engine/template-engine.service';

@Injectable()
export class MailSenderService {
  constructor(
    @Inject(TOKENS.TEMPLATE_ENGINE_SERVICE)
    private readonly templateEngineService: TemplateEngineService,
    @InjectQueue(Queues.EMAILS_WORKER_QUEUE) private readonly emailsWorkerQueue: Queue,
  ) {}

  public async sendMail(
    templateName: TemplateName,
    mailInput: MailInput,
  ): Promise<{ jobId: string }> {
    const finalEmail = this.templateEngineService.generateEmail(
      templateName,
      mailInput,
      mailInput.body,
    );
    const mail: EmailOptions = {
      to: mailInput.to,
      from: mailInput.from,
      bcc: mailInput.bcc,
      replyTo: mailInput.replyTo,
      html: finalEmail,
      subject: this.subjectTemplates(templateName, mailInput.manuscript.manuscriptId),
    };

    const job = await this.emailsWorkerQueue.add(WorkerEvents.SEND_EMAIL, mail, {
      backoff: {
        type: 'exponential',
        delay: 1000 * 5, //5 seconds expo -> 5 ... 10 ... 15
      },
      attempts: 3,
      timeout: 1000 * 60 * 60 * 24, //1 day
    });

    return { jobId: job.id as string };
  }

  private subjectTemplates(templateName: TemplateName, manuscriptId: string) {
    switch (templateName) {
      case TemplateName.revisionRequested:
      case TemplateName.recommendationToRevise:
        return `${manuscriptId}: Revision requested`;
      case TemplateName.invitationToHandleManuscript:
        return `${manuscriptId}: Invitation to handle a manuscript`;
      case TemplateName.manuscriptRejected:
        return `${manuscriptId}: Manuscript rejected`;
      case TemplateName.recommendationToPublish:
        return `${manuscriptId}: Recommendation to publish`;
      case TemplateName.recommendationToReject:
        return `${manuscriptId}: Recommendation to reject`;
      case TemplateName.editorInvitationAccepted:
        return `${manuscriptId}: Editor invitation accepted`;
      case TemplateName.editorDeclined:
        return `${manuscriptId}: Editor Declined`;
      case TemplateName.editorialDecisionReturnedWithComments:
        return `${manuscriptId}: Editorial decision returned with comments`;
      case TemplateName.rejectNoPeerReviews:
      case TemplateName.rejectWithPeerReviews:
        return `${manuscriptId}: Manuscript Rejected`;
    }
  }
}
