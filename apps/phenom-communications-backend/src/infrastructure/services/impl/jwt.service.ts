import { Injectable } from '@nestjs/common';
import { verify } from 'jsonwebtoken';

@Injectable()
export class JwtService {
  verify(signedInput): string | any {
    return verify(signedInput, process.env.JWT_SECRET);
  }
}
