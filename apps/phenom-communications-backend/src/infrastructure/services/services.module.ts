import { Module } from '@nestjs/common';
import { TOKENS } from '../../core/config/constants';
import { MailSenderService } from './impl/mail-sender.service';
import { EmailsQueueModule } from '../queue/emails-queue-module';
import { TemplateEngineModule } from '../template-engine/template-engine.module';
import { JwtService } from './impl/jwt.service';

const providers = [
  {
    provide: TOKENS.MAIL_SENDER_SERVICE,
    useClass: MailSenderService,
  },
  JwtService
];

@Module({
  providers: [...providers],
  exports: [...providers],
  imports: [EmailsQueueModule, TemplateEngineModule],
})
export class ServicesModule {}
