import { OnGlobalQueueCompleted, OnQueueCompleted, Process, Processor } from '@nestjs/bull';
import { Queues, WorkerEvents } from '@hindawi/phenom-communications-types';

@Processor(Queues.EMAILS_WORKER_QUEUE)
export class EventStatusListener {
  @OnGlobalQueueCompleted()
  completed(jobId, result) {
    console.log('job finished ' + jobId + ' == ' + result);
  }
}
