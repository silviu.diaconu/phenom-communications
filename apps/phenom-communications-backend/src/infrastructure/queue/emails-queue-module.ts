import { BullModule, InjectQueue } from '@nestjs/bull';
import { DynamicModule, Module } from '@nestjs/common';
import { Queues } from '@hindawi/phenom-communications-types';
import { EventStatusListener } from './event-status-consumer';
import { Queue } from 'bull';
import { BullAdapter, setQueues } from 'bull-board';

const bullConfig: DynamicModule[] = [
  BullModule.forRoot({
    redis: {
      host: process.env.REDIS_HOST,
      port: parseInt(process.env.REDIS_PORT),
    },
  }),
  BullModule.registerQueue({
    name: Queues.EMAILS_WORKER_QUEUE,
  }),
];

@Module({
  imports: bullConfig,
  providers: [EventStatusListener],
  exports: bullConfig,
})
export class EmailsQueueModule {
  constructor(@InjectQueue(Queues.EMAILS_WORKER_QUEUE) private emailsQueue: Queue) {
    setQueues([new BullAdapter(emailsQueue, { readOnlyMode: false })]);
  }
}
