import { Plugin } from '@nestjs/graphql';
import {
  ApolloServerPlugin,
  GraphQLRequestListener,
} from 'apollo-server-plugin-base';
import { Logger } from '@nestjs/common';

@Plugin()
export class LoggingPlugin implements ApolloServerPlugin {
  requestDidStart(requestContext): GraphQLRequestListener {
    if (requestContext.request.operationName != 'IntrospectionQuery')
      Logger.debug(
        'Request started ... ' +
          (requestContext.request.operationName || 'anonymous'),
        'Graphql-Requests'
      );

    return {
      willSendResponse(responseContext) {
        if (responseContext.response.errors) {
          Logger.warn(
            responseContext.response.errors[0].message,
            'Graphql-Responses'
          );
        }
      },
    };
  }
}
