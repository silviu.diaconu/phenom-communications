import { CanActivate, ExecutionContext, Inject, Injectable, Req } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Logger } from '@nestjs/common';

import { JwtService } from '../../services/impl/jwt.service';

@Injectable()
export class JwtGuard implements CanActivate {
  constructor(
    @Inject(JwtService)
    private readonly jwtService: JwtService,
  ) {}
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const res = context.switchToHttp().getResponse();

    try {
      const signedInput = res.templateInput.signedInput;

      this.jwtService.verify(signedInput);

      return true;
    } catch (e) {
      Logger.warn(e);
      return false;
    }
  }
}
