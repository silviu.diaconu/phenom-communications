import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { LoggingPlugin } from './common/logging.plugin';

import { ServicesModule } from '../services/services.module';
import { TemplateEngineModule } from '../template-engine/template-engine.module';
import { EmailTemplateResolver } from './emailTemplate/email-template.resolver';
import { JwtGuard } from './guards/jwt.guard';

const resolvers = [EmailTemplateResolver];
const guards = [JwtGuard];

@Module({
  imports: [
    ServicesModule,
    TemplateEngineModule,
    GraphQLModule.forRoot({
      autoSchemaFile:
        './apps/phenom-communications-backend/src/infrastructure/graphql/schema.graphql',
    }),
  ],
  providers: [LoggingPlugin, ...guards, ...resolvers],
})
export class GraphqlServerModule {}
