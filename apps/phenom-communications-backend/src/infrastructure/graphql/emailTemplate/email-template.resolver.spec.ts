import { Test, TestingModule } from '@nestjs/testing';
import { GraphQLModule } from '@nestjs/graphql';
import { EmailTemplateResolver } from './email-template.resolver';
import { TOKENS } from '../../../core';

import request from 'supertest';
import { EmailResponse } from '@hindawi/emails';
import jwt from 'jsonwebtoken';
import { TemplateEngineService } from '../../template-engine/template-engine/template-engine.service';
import { TemplateFileReaderService } from '../../template-engine/template-file-reader/template-file-reader.service';
import { EmailTemplateTokensFactory } from '../../template-engine/email-template-tokens/email-template-tokens.service';
import { NestApplication } from '@nestjs/core';
import { CoreModule } from '../../../core/core.module';
import Chance from 'chance';
import { JwtService } from '../../services/impl/jwt.service';
import { MailInput } from './emailTemplate.model';
import { Logger } from '@nestjs/common';
// Instantiate Chance so it can be used

function badRequestGraphql() {
  return `
{
  template(
    templateInput: {
      templateName: invitationToHandleManuscript
      mailInput: {
        logoUrl: "somelogo.png"
        links:{
          unsubscribeLink:"www.unSubscribeMe.pls"
        }
        from: { email: "lucian.harhata@hindawi.com", name: "Luci0" }
        to: { email: "lucian.harhata@hindawi.com", name: "Luci0" }
         manuscript: {
          manuscriptId: "3"
          manuscriptTitle: "title"
          abstract:"Abtract text"
          journalName:"Awesome Journal name",
          submittingAuthor:"Submitting"
          authors: { name: "John", affiliation: "University" }
        }
      }
    }
  ){
   body
  }
}
`;
}

function getInvitationToHandleManuscriptGraphql(signedInput) {
  return `
{
  template(
    templateInput: {
      templateName: invitationToHandleManuscript
      mailInput: {
        logoUrl: "somelogo.png"
         manuscript: {
          manuscriptId: "3"
          manuscriptTitle: "title"
          journalName:"Awesome Journal name",
          submittingAuthor:"Submitting"
        }
      },
      signedInput: "${signedInput}"
    }
  ){
   body, html
  }
}
`;
}
function getSendEmailGraphql(signedInput) {
  return `mutation sendEmail {
  sendEmail(
    templateInput: {
      templateName: invitationToHandleManuscript
      mailInput: {
        logoUrl: "somelogo.png"
        manuscript: {
          manuscriptId: "3"
          manuscriptTitle: "title"
          journalName:"Awesome Journal name",
          submittingAuthor:"Submitting"
        }
        body:["XXX1", "XXX2", "XXX3"]
      },

      signedInput: "${signedInput}"
    }
  ) {
    success
    statusCode
    messages
  }
}
`;
}
const chance = new Chance();

describe('EmailTemplateResolver', () => {
  let moduleRef: TestingModule;
  let app: NestApplication;

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [
        CoreModule,
        GraphQLModule.forRoot({
          autoSchemaFile: './schema_tests.graphql',
        }),
      ],
      providers: [
        EmailTemplateResolver,
        JwtService,
        {
          provide: TOKENS.TEMPLATE_ENGINE_SERVICE,
          useClass: TemplateEngineService,
        },
        { provide: TOKENS.TEMPLATE_FILE_READER_SERVICE, useClass: TemplateFileReaderService },
        { provide: TOKENS.EMAIL_TEMPLATE_TOKENS_FACTORY, useClass: EmailTemplateTokensFactory },

        {
          provide: TOKENS.MAIL_SENDER_SERVICE,
          useValue: {
            //todo extract mocks and use interfaces
            sendMail: () => {
              return {
                messages: [],
                statusCode: '200',
                success: true,
              } as EmailResponse;
            },
          },
        },
      ],
    }).compile();

    app = moduleRef.createNestApplication();

    TemplateFileReaderService.TEMPLATES_PATH = '../../../assets/templates';
    TemplateFileReaderService.DEFAULTS_PATH = '../../../assets/defaults';

    await app.init();
  });

  describe('template', () => {
    it('should fail if signed input not provided', () => {
      const badRequestGrahql = badRequestGraphql();
      return request(app.getHttpServer())
        .post('/graphql')
        .send({
          operationName: null,
          query: badRequestGrahql,
        })
        .expect(400)
        .expect(({ body }) => {
          expect(body.errors[0].message).toStrictEqual(
            'Field "TemplateInput.signedInput" of required type "String!" was not provided.',
          );
        });
    });

    it('should fail if signed inputs is null', () => {
      const plainInput = undefined;

      const queryInvitationToHandleManuscript = getInvitationToHandleManuscriptGraphql('');
      return request(app.getHttpServer())
        .post('/graphql')
        .send({
          operationName: null,
          query: queryInvitationToHandleManuscript,
        })
        .expect(200)
        .expect(({ body }) => {
          expect(body.errors[0].message).toStrictEqual('Forbidden resource');
        });
    });

    it('should fail if request is not signed', () => {
      const plainInput = 'xxxxxxxx';

      const queryInvitationToHandleManuscript = getInvitationToHandleManuscriptGraphql(plainInput);
      return request(app.getHttpServer())
        .post('/graphql')
        .send({
          operationName: null,
          query: queryInvitationToHandleManuscript,
        })
        .expect(200)
        .expect(({ body }) => {
          expect(body.errors[0].message).toStrictEqual('Forbidden resource');
        });
    });

    it('should fail if request is signed with different key', () => {
      const signedInputWithFakekey = jwt.sign({ foo: 'bar' }, `fakeKey${chance.string()}`);

      const queryInvitationToHandleManuscript = getInvitationToHandleManuscriptGraphql(
        signedInputWithFakekey,
      );
      return request(app.getHttpServer())
        .post('/graphql')
        .send({
          operationName: null,
          query: queryInvitationToHandleManuscript,
        })
        .expect(200)
        .expect(({ body }) => {
          expect(body.errors[0].message).toStrictEqual('Forbidden resource');
        });
    });

    it('should return InvitationToHandleManuscript template data', () => {
      const tokens: MailInput = {
        links: {
          unsubscribeLink: 'www.unSubscribeMe.pls',
          agreeLink: 'www.agree.pls',
          declineLink: 'www.decline.pls',
          reviewLink: 'www.review.pls',
        },
        from: { email: 'lucian.harhata@hindawi.com', name: 'Luci0' },
        to: { email: 'lucian.harhata@hindawi.com', name: 'Luci0' },
        manuscript: {
          manuscriptId: '3',
          abstract: 'Abtract text',
          // manuscriptTitle: 'title',
          // submittingAuthor: 'Submitting',
          // journalName: 'Awesome Journal name',
          authors: [{ name: 'John', affiliation: 'University' }],
        },
      };
      const signedTokens = jwt.sign(tokens, process.env.JWT_SECRET);

      const queryInvitationToHandleManuscript = getInvitationToHandleManuscriptGraphql(
        signedTokens,
      );

      return request(app.getHttpServer())
        .post('/graphql')
        .send({
          operationName: null,
          query: queryInvitationToHandleManuscript,
        })
        .expect(({ body }) => {
          expect(body.data.template.body).toBeTruthy();
          expect(body.data.template.html).toBeTruthy();
        })
        .expect(200);
    });
  });

  describe('sendEmail', () => {
    it('should send email', () => {
      const tokens: MailInput = {
        logoUrl: 'somelogo.png',
        links: {
          unsubscribeLink: 'www.unSubscribeMe.pls',
          agreeLink: 'www.agree.pls',
          declineLink: 'www.decline.pls',
          reviewLink: 'www.review.pls',
        },
        from: { email: 'lucian.harhata@hindawi.com', name: 'Luci0' },
        to: { email: 'luci00@gmail.com', name: 'Luci0' },
        replyTo: { email: 'luci00@gmail.com', name: 'Luci0' },
        manuscript: {
          manuscriptId: '3',
          abstract: 'Abtract text',
          authors: [{ name: 'John', affiliation: 'University' }],
        },
      };
      const signedTokens = jwt.sign(tokens, process.env.JWT_SECRET);
      Logger.debug(signedTokens);

      return request(app.getHttpServer())
        .post('/graphql')
        .send({ query: getSendEmailGraphql(signedTokens) })
        .expect(({ body }) => {
          expect(body.data.sendEmail).toStrictEqual({
            success: true,
            statusCode: '200',
            messages: null,
          });
        })
        .expect(200);
    });
  });
});
