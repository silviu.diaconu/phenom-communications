import { Inject, Logger, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import merge from 'deepmerge';
import {
  EditableInput,
  MailInput,
  SendMailResponse,
  Template,
  TemplateInput,
} from './emailTemplate.model';
import { MailSenderService } from '../../services/impl/mail-sender.service';
import { TOKENS } from '../../../core';
import { TemplateEngineService } from '../../template-engine/template-engine/template-engine.service';
import { JwtGuard } from '../guards/jwt.guard';
import { JwtService } from '../../services/impl/jwt.service';
import { MailInputDto } from '@hindawi/phenom-communications-types';

@UseGuards(JwtGuard)
@Resolver(() => Template)
export class EmailTemplateResolver {
  constructor(
    @Inject(TOKENS.MAIL_SENDER_SERVICE)
    private readonly mailSenderService: MailSenderService,
    @Inject(TOKENS.TEMPLATE_ENGINE_SERVICE)
    private readonly templateEngineService: TemplateEngineService,
    @Inject(JwtService)
    private readonly jwtService: JwtService,
  ) {}

  @Query((_returns) => Template!)
  template(@Args('templateInput') input: TemplateInput): Template {
    const { templateName, mailInput, signedInput } = input;
    const mergedInput = this.computeMergedInput(mailInput, signedInput);

    return this.templateEngineService.getTemplate(templateName, mergedInput);
  }

  private computeMergedInput(mailInput: EditableInput, signedInput: string): MailInput {
    const editableInput = mailInput;

    const decodedInput: MailInputDto = this.jwtService.verify(signedInput);

    return merge(editableInput, decodedInput) as MailInput;
  }

  @Mutation((_returns) => SendMailResponse!)
  async sendEmail(@Args('templateInput') input: TemplateInput): Promise<SendMailResponse> {
    const { templateName, mailInput, signedInput } = input;
    try {
      const mergedInput: MailInput = this.computeMergedInput(mailInput, signedInput);

      const { jobId } = await this.mailSenderService.sendMail(templateName, mergedInput);

      return { success: true, statusCode: '200', jobId: jobId };
    } catch (error) {
      return {
        messages: [error.message],
        statusCode: 'customError',
        success: false,
      };
    }
  }
}
