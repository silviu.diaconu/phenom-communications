import {
  AuthorDto,
  ContactDto,
  MailInputDto,
  ManuscriptDto,
  SendMailResponseDto,
  TemplateInputDto,
  TemplateName,
  LinksDto,
  EditableInputDto,
  PublisherName,
} from '@hindawi/phenom-communications-types';
import { Field, InputType, ObjectType, registerEnumType } from '@nestjs/graphql';

registerEnumType(TemplateName, {
  name: 'TemplateName',
});

@InputType()
export class ContactInput implements ContactDto {
  @Field()
  email: string;
  @Field()
  name: string;
}

@InputType()
export class AuthorInput implements AuthorDto {
  @Field()
  name: string;
  @Field()
  affiliation: string;
}

@InputType({})
export class ManuscriptInput implements ManuscriptDto {
  @Field()
  manuscriptId: string;
  @Field({ nullable: true })
  manuscriptTitle?: string;
  @Field((_type) => [AuthorInput], { nullable: true })
  authors?: AuthorInput[];
  @Field({ nullable: true })
  journalName?: string;
  @Field({ nullable: true })
  abstract?: string;
  @Field({ nullable: true })
  submittingAuthor?: string;
  @Field({ nullable: true })
  comments?: string;
}

@InputType({})
export class LinksInput implements LinksDto {
  @Field({ nullable: true })
  agreeLink?: string;
  @Field({ nullable: true })
  declineLink?: string;
  @Field({ nullable: true })
  manuscriptDetailsLink?: string;
  @Field({ nullable: true })
  unsubscribeLink?: string;
  @Field({ nullable: true })
  reviewLink?: string;
}

export class MailInput implements MailInputDto {
  logoUrl?: string;
  to?: ContactInput;
  from?: ContactInput;
  bcc?: ContactInput[];
  replyTo?: ContactInput;
  manuscript: ManuscriptInput;
  links?: LinksInput;
  body?: string[];
  publisherName?: PublisherName;
}

@InputType()
export class EditableInput implements EditableInputDto {
  @Field((_type) => ManuscriptInput)
  manuscript: ManuscriptDto;

  @Field((_type) => [String!]!, { nullable: true })
  body: string[];

  @Field((_type) => String!)
  logoUrl: string;
}

@InputType()
export class TemplateInput implements TemplateInputDto {
  @Field((_type) => TemplateName!)
  templateName: TemplateName;
  @Field((_type) => EditableInput!)
  mailInput: EditableInput;
  @Field()
  signedInput: string;
}

@ObjectType()
export class Template {
  @Field()
  html: string;
  @Field((_type) => [String!]!)
  body: string[];
}

@ObjectType()
export class SendMailResponse implements SendMailResponseDto {
  @Field()
  success: boolean;

  @Field({ nullable: true })
  statusCode: string;

  @Field({ nullable: true })
  jobId?: string;

  @Field((_type) => [String], { nullable: true })
  messages?: string[];
}
