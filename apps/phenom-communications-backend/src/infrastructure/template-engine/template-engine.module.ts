import { Module } from '@nestjs/common';
import { TOKENS } from '../../core';
import { CoreModule } from '../../core/core.module';
import { EmailTemplateTokensFactory } from './email-template-tokens/email-template-tokens.service';
import { TemplateEngineService } from './template-engine/template-engine.service';
import { TemplateFileReaderService } from './template-file-reader/template-file-reader.service';

const providers = [
  {
    provide: TOKENS.EMAIL_TEMPLATE_TOKENS_FACTORY,
    useClass: EmailTemplateTokensFactory,
  },
  {
    provide: TOKENS.TEMPLATE_FILE_READER_SERVICE,
    useClass: TemplateFileReaderService,
  },
  {
    provide: TOKENS.TEMPLATE_ENGINE_SERVICE,
    useClass: TemplateEngineService,
  },
];

@Module({
  imports: [CoreModule],
  providers: [...providers],
  exports: [
    {
      provide: TOKENS.TEMPLATE_ENGINE_SERVICE,
      useClass: TemplateEngineService,
    },
  ],
})
export class TemplateEngineModule {}
