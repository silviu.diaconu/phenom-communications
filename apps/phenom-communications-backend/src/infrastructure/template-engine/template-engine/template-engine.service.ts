import { PublisherName, TemplateName } from '@hindawi/phenom-communications-types';
import { Inject, Injectable } from '@nestjs/common';
import handlebars, { HelperOptions } from 'handlebars';
import { MailInput, Template } from '../../graphql/emailTemplate/emailTemplate.model';
import { TOKENS } from '../../../core/config/constants';
import { EmailTemplateTokensFactory } from '../email-template-tokens/email-template-tokens.service';
import { TemplateFileReaderService } from '../template-file-reader/template-file-reader.service';

handlebars.registerHelper('inc', function (value: string) {
  return parseInt(value) + 1;
});

handlebars.registerHelper(
  'ifEquals',
  function (arg1: string, arg2: string, options: HelperOptions) {
    return arg1 == arg2 ? options.fn(this) : options.inverse(this);
  },
);

@Injectable()
export class TemplateEngineService {
  constructor(
    @Inject(TOKENS.TEMPLATE_FILE_READER_SERVICE)
    private readonly templateReaderService: TemplateFileReaderService,
    @Inject(TOKENS.EMAIL_TEMPLATE_TOKENS_FACTORY)
    private readonly tokensFactory: EmailTemplateTokensFactory,
  ) {}

  public getTemplate(templateName: TemplateName, mailInput: MailInput): Template {
    const body = this.generateBody(templateName, mailInput);
    const html = this.generateEmail(templateName, mailInput, body);

    return {
      html,
      body,
    };
  }

  public generateEmail(
    templateName: TemplateName,
    mailInput: MailInput,
    emailBodies: string[],
  ): string {
    let emailBodyContent = [...emailBodies];
    const emailHtmlTemplate = this.templateReaderService.getEmailHtmlFile(templateName);
    const handlebarsEmail = handlebars.compile(emailHtmlTemplate);

    const templateTokens = this.tokensFactory.create(mailInput, templateName);

    if (emailBodies.length === 0) {
      emailBodyContent = this.generateBody(templateName, mailInput);
    }
    const signature = emailBodyContent.pop();
    templateTokens[`signature`] = signature;
    emailBodyContent.forEach((b, idx) => {
      return (templateTokens[`body${idx + 1}`] = b);
    });
    return handlebarsEmail(templateTokens);
  }

  private generateBody(templateName: TemplateName, mailInput: MailInput): string[] {
    const bodyTokens = this.tokensFactory.create(mailInput, templateName);
    const bodiesFiles = this.templateReaderService.getBodyContentFiles(templateName);
    return bodiesFiles.map((b) => {
      const handlebarBody = handlebars.compile(b);
      return handlebarBody(bodyTokens);
    });
  }
}
