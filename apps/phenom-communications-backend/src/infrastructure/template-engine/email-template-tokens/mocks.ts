import { MailInput } from '../../graphql/emailTemplate/emailTemplate.model';

export const mailInputMock: MailInput = {
  to: {
    email: 'author@hindawi.com',
    name: 'Author',
  },
  from: {
    email: 'triage.editor@hindawi.com',
    name: 'Triage Editor',
  },
  manuscript: {
    manuscriptId: '2345',
    manuscriptTitle: 'test',
    journalName: 'Journal Name',
    authors: [
      { name: 'First Author', affiliation: 'Multiverse' },
      { name: 'Second Author', affiliation: 'Multiverse 2' },
    ],
    abstract: 'abstract',
    submittingAuthor: 'Submitting Author',
    comments: 'This is my comment',
  },
  logoUrl: 'www.google.com',
  links: {
    unsubscribeLink: 'www.unsubscribe.com',
    manuscriptDetailsLink: 'www.manuscriptdetails.com',
    agreeLink: 'www.agree.com',
    declineLink: 'www.decline.com',
    reviewLink: 'www.review.com',
  },
};

export const reviewMailTemplatesTokensMock = {
  fromEmail: 'triage.editor@hindawi.com',
  fromName: 'Triage Editor',
  toEmail: 'author@hindawi.com',
  toName: 'Author',
  journalName: 'Journal Name',
  manuscriptTitle: 'test',
  submittingAuthorName: 'Submitting Author',
  authorsList: ['First Author', 'Second Author'],
  authorsAffiliations: ['Multiverse', 'Multiverse 2'],
  logoUrl: 'www.google.com',
  abstract: 'abstract',
  links: {
    unsubscribeLink: 'www.unsubscribe.com',
    manuscriptDetailsLink: 'www.manuscriptdetails.com',
    agreeLink: 'www.agree.com',
    declineLink: 'www.decline.com',
    reviewLink: 'www.review.com',
  },
  comments: 'This is my comment',
};
