import { TemplateName } from '@hindawi/phenom-communications-types';
import { Test } from '@nestjs/testing';
import { AutomapperModule } from 'nestjsx-automapper';
import { TOKENS } from '../../../core';
import { CoreModule } from '../../../core/core.module';
import { EmailTemplateTokensFactory } from './email-template-tokens.service';
import { mailInputMock, reviewMailTemplatesTokensMock } from './mocks';

describe('EmailTemplateTokensService', () => {
  let templateTokensFactoryService: EmailTemplateTokensFactory;
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        {
          provide: TOKENS.EMAIL_TEMPLATE_TOKENS_FACTORY,
          useClass: EmailTemplateTokensFactory,
        },
      ],
      imports: [CoreModule],
    }).compile();

    const automapperModule = moduleRef.get<AutomapperModule>(AutomapperModule);
    automapperModule.onModuleInit();

    templateTokensFactoryService = await moduleRef.resolve<EmailTemplateTokensFactory>(
      TOKENS.EMAIL_TEMPLATE_TOKENS_FACTORY,
    );
  });

  it('Should generate reviewMailTemplatesTokens', () => {
    const tokens = templateTokensFactoryService.create(
      mailInputMock,
      TemplateName.revisionRequested,
    );
    expect(tokens).toEqual(reviewMailTemplatesTokensMock);
  });
});
