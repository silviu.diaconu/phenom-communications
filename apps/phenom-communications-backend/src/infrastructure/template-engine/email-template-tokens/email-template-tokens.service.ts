import { Injectable, NotFoundException } from '@nestjs/common';
import { AutoMapper, InjectMapper } from 'nestjsx-automapper';
import { MailInput } from '../../graphql/emailTemplate/emailTemplate.model';

import { TemplateName } from '@hindawi/phenom-communications-types';
import { ReviewMailTemplatesTokens } from '../../../core';

@Injectable()
export class EmailTemplateTokensFactory {
  constructor(@InjectMapper() private readonly mapper: AutoMapper) {}
  create(mailInput: MailInput, templateName: TemplateName) {
    switch (templateName) {
      case TemplateName.revisionRequested:
      case TemplateName.invitationToHandleManuscript:
      case TemplateName.manuscriptRejected:
      case TemplateName.recommendationToPublish:
      case TemplateName.recommendationToReject:
      case TemplateName.editorInvitationAccepted:
      case TemplateName.editorDeclined:
      case TemplateName.editorialDecisionReturnedWithComments:
      case TemplateName.rejectNoPeerReviews:
      case TemplateName.rejectWithPeerReviews:
      case TemplateName.recommendationToRevise:
        return this.mapper.map(mailInput, ReviewMailTemplatesTokens, MailInput);
      default:
        throw new NotFoundException('Template does not exists');
    }
  }
}
