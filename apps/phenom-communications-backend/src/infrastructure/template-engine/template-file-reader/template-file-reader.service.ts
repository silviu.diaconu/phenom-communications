import { TemplateName } from '@hindawi/phenom-communications-types';
import { Injectable, Req } from '@nestjs/common';
import { readdirSync, readFileSync } from 'fs';
import { join, dirname } from 'path';

@Injectable()
export class TemplateFileReaderService {
  static TEMPLATES_PATH = '/assets/templates';
  static DEFAULTS_PATH = '/assets/defaults';

  getBodyContentFiles(
    templateName: TemplateName,
    path = TemplateFileReaderService.DEFAULTS_PATH,
  ): string[] {
    const files = readdirSync(join(__dirname, path));

    const signature = readFileSync(join(__dirname, path + '/signature.html')).toString();

    const matchingFiles = files.filter((fileName) => fileName.includes(templateName));
    const mailBodies = matchingFiles.map((fileName) =>
      readFileSync(join(__dirname, `${path}/${fileName}`), { encoding: 'utf-8' }),
    );
    mailBodies.push(signature);

    return mailBodies;
  }

  getEmailHtmlFile(
    templateName: TemplateName,
    path = TemplateFileReaderService.TEMPLATES_PATH,
  ): string {
    const filePath = join(__dirname, `${path}/${templateName}.html`);
    return readFileSync(filePath, { encoding: 'utf-8' });
  }
}
