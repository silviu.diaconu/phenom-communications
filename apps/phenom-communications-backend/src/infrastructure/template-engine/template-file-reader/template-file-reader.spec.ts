import { TemplateName } from '@hindawi/phenom-communications-types';
import { Test } from '@nestjs/testing';
import { TOKENS } from '../../../core';
import { TemplateFileReaderService } from './template-file-reader.service';

describe('Template File Reader', () => {
  let templateFileReaderService: TemplateFileReaderService;
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        {
          provide: TOKENS.TEMPLATE_FILE_READER_SERVICE,
          useClass: TemplateFileReaderService,
        },
      ],
    }).compile();

    templateFileReaderService = await moduleRef.resolve<TemplateFileReaderService>(
      TOKENS.TEMPLATE_FILE_READER_SERVICE,
    );
  });

  it('Should read all body files for template', () => {
    const body = templateFileReaderService.getBodyContentFiles(
      TemplateName.invitationToHandleManuscript,
      './fixtures/body-contents',
    );
    expect(body.length).toEqual(3);
  });

  it('Should read template', () => {
    const template = templateFileReaderService.getEmailHtmlFile(
      TemplateName.revisionRequested,
      './fixtures/templates',
    );

    expect(template.length > 0).toBeTruthy();
  });
});
