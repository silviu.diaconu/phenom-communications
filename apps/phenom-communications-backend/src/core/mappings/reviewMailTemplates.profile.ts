import { PublisherName } from '@hindawi/phenom-communications-types';
import { AutoMapper, mapFrom, Profile, ProfileBase } from 'nestjsx-automapper';
import { MailInput } from '../../infrastructure/graphql/emailTemplate/emailTemplate.model';

export class ReviewMailTemplatesTokens {
  toName: string;
  fromName: string;
  toEmail: string;
  fromEmail: string;
  logoUrl: string;
  journalName: string;
  manuscriptTitle: string;
  submittingAuthorName: string;
  authorsList: string[];
  authorsAffiliations: string[];
  abstract: string;
  links: {
    unsubscribeLink: string;
    manuscriptDetailsLink: string;
    agreeLink: string;
    declineLink: string;
    reviewLink: string;
  };
  comments: string;
  publisherName: PublisherName;
}

@Profile()
export class ReviewMailTemplatesProfile extends ProfileBase {
  constructor(mapper: AutoMapper) {
    super();
    mapper

      .createMap(MailInput, ReviewMailTemplatesTokens, { useUndefined: true })
      .forMember(
        (dest) => dest.fromEmail,
        mapFrom((source) => source.from.email),
      )
      .forMember(
        (dest) => dest.fromName,
        mapFrom((source) => source.from.name),
      )
      .forMember(
        (dest) => dest.toEmail,
        mapFrom((source) => source.to.email),
      )
      .forMember(
        (dest) => dest.toName,
        mapFrom((source) => source.to.name),
      )
      .forMember(
        (dest) => dest.logoUrl,
        mapFrom((source) => source.logoUrl),
      )
      .forMember(
        (dest) => dest.journalName,
        mapFrom((source) => source.manuscript.journalName),
      )
      .forMember(
        (dest) => dest.abstract,
        mapFrom((source) => source.manuscript.abstract),
      )
      .forMember(
        (dest) => dest.authorsList,
        mapFrom((source) => source.manuscript.authors.map((a) => a.name)),
      )
      .forMember(
        (dest) => dest.authorsAffiliations,
        mapFrom((source) => source.manuscript.authors.map((a) => a.affiliation)),
      )
      .forMember(
        (dest) => dest.links.unsubscribeLink,
        mapFrom((source) => source.links.unsubscribeLink),
      )
      .forMember(
        (dest) => dest.links.agreeLink,
        mapFrom((source) => source.links.agreeLink),
      )
      .forMember(
        (dest) => dest.links.declineLink,
        mapFrom((source) => source.links.declineLink),
      )
      .forMember(
        (dest) => dest.links.reviewLink,
        mapFrom((source) => source.links.reviewLink),
      )
      .forMember(
        (dest) => dest.links.manuscriptDetailsLink,
        mapFrom((source) => source.links.manuscriptDetailsLink),
      )
      .forMember(
        (dest) => dest.submittingAuthorName,
        mapFrom((source) => source.manuscript.submittingAuthor),
      )
      .forMember(
        (dest) => dest.manuscriptTitle,
        mapFrom((source) => source.manuscript.manuscriptTitle),
      )
      .forMember(
        (dest) => dest.comments,
        mapFrom((source) => source.manuscript.comments),
      )
      .forMember(
        (dest) => dest.publisherName,
        mapFrom((source) => source.publisherName),
      );
  }
}
