import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtConfig } from './jwt.config';

const providers = [JwtConfig];

@Module({
  imports: [ConfigModule.forRoot()],
  providers: providers,
  exports: providers,
})
export class ConfigsModule {}
