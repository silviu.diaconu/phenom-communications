import { Injectable } from '@nestjs/common';


@Injectable()
export class JwtConfig {
  secret() {
    return process.env.JWT_SECRET;
  }
}
