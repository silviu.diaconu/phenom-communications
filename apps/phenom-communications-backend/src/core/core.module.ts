import { Module } from '@nestjs/common';
import { AutomapperModule } from 'nestjsx-automapper';
import './mappings/reviewMailTemplates.profile';

import { ConfigsModule } from './config/configs.module';
//todo Don't forget to import new profiles here, after adding them
const automapperModule = AutomapperModule.withMapper('default', {
  skipUnmappedAssertion: true,
  useUndefined: true,
});
@Module({
  imports: [automapperModule, ConfigsModule],
  exports: [automapperModule],
})
export class CoreModule {}
