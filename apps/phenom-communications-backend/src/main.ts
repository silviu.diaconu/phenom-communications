/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common';
import { NestApplication, NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { router } from 'bull-board';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
import basicAuth from 'express-basic-auth';

async function bootstrap() {
  const port = process.env.PORT || 3333;
  const globalPrefix = 'api';

  const app: NestApplication = await NestFactory.create<NestApplication>(AppModule, {
    logger: true,
  });

  app.setGlobalPrefix(globalPrefix);
  await addExpressMiddlewares(app);
  await app.listen(port, () => {
    Logger.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });
}

async function addExpressMiddlewares(app: NestApplication) {
  const expressApp = new ExpressAdapter(app);

  //bull-ui basic auth
  expressApp.use(
    '/admin/queues',
    basicAuth({
      challenge: true,
      users: { admin: process.env.BULL_UI_PASSWORD },
    }),
    router,
  );
}

bootstrap();
