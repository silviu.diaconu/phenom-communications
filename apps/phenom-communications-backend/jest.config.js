module.exports = {
  displayName: 'phenom-communications-backend',
  preset: '../../jest.preset.js',
  globals: {
    'ts-jest': {
      tsConfig: '<rootDir>/tsconfig.spec.json',
    },
  },
  transform: {
    '^.+\\.[tj]s$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'js', 'html'],
  coverageDirectory: '../../coverage/apps/phenom-communications-backend',
  setupFiles: ['<rootDir>/jest/setJestEnv.ts'],
  testEnvironment: 'node'
};
