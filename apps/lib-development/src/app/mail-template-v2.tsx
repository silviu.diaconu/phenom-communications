import React, { useState } from 'react';
import { MailTemplate } from '@hindawi/phenom-mail-template';
import { Card } from '@hindawi/phenom-ui';

const html =
  '<!doctype html>\n' +
  '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">\n' +
  '\n' +
  '<head>\n' +
  '  <title>\n' +
  '  </title>\n' +
  '  <!--[if !mso]><!-->\n' +
  '  <meta http-equiv="X-UA-Compatible" content="IE=edge">\n' +
  '  <!--<![endif]-->\n' +
  '  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n' +
  '  <meta name="viewport" content="width=device-width, initial-scale=1">\n' +
  '  <style type="text/css">\n' +
  '    #outlook a {\n' +
  '      padding: 0;\n' +
  '    }\n' +
  '\n' +
  '    body {\n' +
  '      margin: 0;\n' +
  '      padding: 0;\n' +
  '      -webkit-text-size-adjust: 100%;\n' +
  '      -ms-text-size-adjust: 100%;\n' +
  '    }\n' +
  '\n' +
  '    table,\n' +
  '    td {\n' +
  '      border-collapse: collapse;\n' +
  '      mso-table-lspace: 0pt;\n' +
  '      mso-table-rspace: 0pt;\n' +
  '    }\n' +
  '\n' +
  '    img {\n' +
  '      border: 0;\n' +
  '      height: auto;\n' +
  '      line-height: 100%;\n' +
  '      outline: none;\n' +
  '      text-decoration: none;\n' +
  '      -ms-interpolation-mode: bicubic;\n' +
  '    }\n' +
  '\n' +
  '    p {\n' +
  '      display: block;\n' +
  '      margin: 13px 0;\n' +
  '    }\n' +
  '  </style>\n' +
  '  <!--[if mso]>\n' +
  '        <xml>\n' +
  '        <o:OfficeDocumentSettings>\n' +
  '          <o:AllowPNG/>\n' +
  '          <o:PixelsPerInch>96</o:PixelsPerInch>\n' +
  '        </o:OfficeDocumentSettings>\n' +
  '        </xml>\n' +
  '        <![endif]-->\n' +
  '  <!--[if lte mso 11]>\n' +
  '        <style type="text/css">\n' +
  '          .mj-outlook-group-fix { width:100% !important; }\n' +
  '        </style>\n' +
  '        <![endif]-->\n' +
  '  <!--[if !mso]><!-->\n' +
  '  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">\n' +
  '  <style type="text/css">\n' +
  '    @import url(https://fonts.googleapis.com/css?family=Roboto:300,400,500,700);\n' +
  '  </style>\n' +
  '  <!--<![endif]-->\n' +
  '  <style type="text/css">\n' +
  '    @media only screen and (min-width:480px) {\n' +
  '      .mj-column-per-100 {\n' +
  '        width: 100% !important;\n' +
  '        max-width: 100%;\n' +
  '      }\n' +
  '    }\n' +
  '  </style>\n' +
  '  <style media="screen and (min-width:480px)">\n' +
  '    .moz-text-html .mj-column-per-100 {\n' +
  '      width: 100% !important;\n' +
  '      max-width: 100%;\n' +
  '    }\n' +
  '  </style>\n' +
  '  <style type="text/css">\n' +
  '    @media only screen and (max-width:480px) {\n' +
  '      table.mj-full-width-mobile {\n' +
  '        width: 100% !important;\n' +
  '      }\n' +
  '\n' +
  '      td.mj-full-width-mobile {\n' +
  '        width: auto !important;\n' +
  '      }\n' +
  '    }\n' +
  '  </style>\n' +
  '</head>\n' +
  '\n' +
  '<body style="word-spacing:normal;background-color:#f4f4f4;">\n' +
  '  <div style="background-color:#f4f4f4;">\n' +
  '    <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\n' +
  '    <div style="margin:0px auto;max-width:600px;">\n' +
  '      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">\n' +
  '        <tbody>\n' +
  '          <tr>\n' +
  '            <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">\n' +
  '              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->\n' +
  '              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\n' +
  '                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">\n' +
  '                  <tbody>\n' +
  '                    <tr>\n' +
  '                      <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">\n' +
  '                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">\n' +
  '                          <tbody>\n' +
  '                            <tr>\n' +
  '                              <td style="width:50px;">\n' +
  '                                <img height="auto" src="https://ci4.googleusercontent.com/proxy/l2B7lLP1vmAu0TBLXze-DpzWidHLv5sTh4ADY2dUieFUEi7IrqsXHtipuFv_ZYVIdIw&#x3D;s0-d-e1-ft#https://i.imgur.com/dCbJ3C1.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="50" />\n' +
  '                              </td>\n' +
  '                            </tr>\n' +
  '                          </tbody>\n' +
  '                        </table>\n' +
  '                      </td>\n' +
  '                    </tr>\n' +
  '                  </tbody>\n' +
  '                </table>\n' +
  '              </div>\n' +
  '              <!--[if mso | IE]></td></tr></table><![endif]-->\n' +
  '            </td>\n' +
  '          </tr>\n' +
  '        </tbody>\n' +
  '      </table>\n' +
  '    </div>\n' +
  '    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\n' +
  '    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">\n' +
  '      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">\n' +
  '        <tbody>\n' +
  '          <tr>\n' +
  '            <td style="direction:ltr;font-size:0px;padding:25px 25px 0 25px;text-align:center;">\n' +
  '              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:550px;" ><![endif]-->\n' +
  '              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">\n' +
  '                <!--[if mso | IE]><table border="0" cellpadding="0" cellspacing="0" role="presentation" ><tr><td style="vertical-align:top;width:550px;" ><![endif]-->\n' +
  '                <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\n' +
  '                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">\n' +
  '                    <tbody>\n' +
  '                      <tr>\n' +
  '                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">\n' +
  '                          <div style="font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;text-align:left;color:#626262;">Dear Luci0,</div>\n' +
  '                        </td>\n' +
  '                      </tr>\n' +
  '                    </tbody>\n' +
  '                  </table>\n' +
  '                </div>\n' +
  '                <!--[if mso | IE]></td></tr></table><![endif]-->\n' +
  '              </div>\n' +
  '              <!--[if mso | IE]></td></tr></table><![endif]-->\n' +
  '            </td>\n' +
  '          </tr>\n' +
  '        </tbody>\n' +
  '      </table>\n' +
  '    </div>\n' +
  '    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\n' +
  '    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">\n' +
  '      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">\n' +
  '        <tbody>\n' +
  '          <tr>\n' +
  '            <td style="direction:ltr;font-size:0px;padding:0px 25px;text-align:center;">\n' +
  '              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:550px;" ><![endif]-->\n' +
  '              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">\n' +
  '                <!--[if mso | IE]><table border="0" cellpadding="0" cellspacing="0" role="presentation" ><tr><td style="vertical-align:top;width:550px;" ><![endif]-->\n' +
  '                <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\n' +
  '                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">\n' +
  '                    <tbody>\n' +
  '                      <tr>\n' +
  '                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">\n' +
  '                          <div style="font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;text-align:left;color:#626262;">In order for your submission "Manuscript title" to Journal name to proceed to the review process, there needs to be a revision. <p>Reason & Details:</p>\n' +
  '                          </div>\n' +
  '                        </td>\n' +
  '                      </tr>\n' +
  '                      <tr>\n' +
  '                        <td align="left" class="editable-area" style="font-size:0px;padding:10px 25px;word-break:break-word;">\n' +
  '                          <div style="font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;text-align:left;color:#626262;"><strong>This is my comment</strong>\n' +
  '\n' +
  '</div>\n' +
  '                        </td>\n' +
  '                      </tr>\n' +
  '                      <tr>\n' +
  '                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">\n' +
  '                          <div style="font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;text-align:left;color:#626262;">For more information about what is required, please click the link below.</div>\n' +
  '                        </td>\n' +
  '                      </tr>\n' +
  '                    </tbody>\n' +
  '                  </table>\n' +
  '                </div>\n' +
  '                <!--[if mso | IE]></td></tr></table><![endif]-->\n' +
  '              </div>\n' +
  '              <!--[if mso | IE]></td></tr></table><![endif]-->\n' +
  '            </td>\n' +
  '          </tr>\n' +
  '        </tbody>\n' +
  '      </table>\n' +
  '    </div>\n' +
  '    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\n' +
  '    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">\n' +
  '      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">\n' +
  '        <tbody>\n' +
  '          <tr>\n' +
  '            <td style="direction:ltr;font-size:0px;padding:25px;text-align:center;">\n' +
  '              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:550px;" ><![endif]-->\n' +
  '              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\n' +
  '                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">\n' +
  '                  <tbody>\n' +
  '                    <tr>\n' +
  '                      <td align="center" vertical-align="middle" style="font-size:0px;padding:12px 18px;word-break:break-word;">\n' +
  '                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;">\n' +
  '                          <tr>\n' +
  '                            <td align="center" bgcolor="#63a945" role="presentation" style="border:none;border-radius:5px;cursor:auto;mso-padding-alt:10px 25px;background:#63a945;" valign="middle">\n' +
  '                              <a href="http://localhost:3000/details/e827f817-3885-4f2b-a633-859977460052/24c08a43-eeb4-4905-bf52-be3b609d91aa" style="display:inline-block;background:#63a945;color:#ffffff;font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;font-size:14px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:5px;" target="_blank"> MANUSCRIPT DETAILS </a>\n' +
  '                            </td>\n' +
  '                          </tr>\n' +
  '                        </table>\n' +
  '                      </td>\n' +
  '                    </tr>\n' +
  '                  </tbody>\n' +
  '                </table>\n' +
  '              </div>\n' +
  '              <!--[if mso | IE]></td></tr></table><![endif]-->\n' +
  '            </td>\n' +
  '          </tr>\n' +
  '        </tbody>\n' +
  '      </table>\n' +
  '    </div>\n' +
  '    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\n' +
  '    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">\n' +
  '      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">\n' +
  '        <tbody>\n' +
  '          <tr>\n' +
  '            <td style="direction:ltr;font-size:0px;padding:0px 25px 25px 25px;text-align:center;">\n' +
  '              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:550px;" ><![endif]-->\n' +
  '              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">\n' +
  '                <!--[if mso | IE]><table border="0" cellpadding="0" cellspacing="0" role="presentation" ><tr><td style="vertical-align:top;width:550px;" ><![endif]-->\n' +
  '                <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\n' +
  '                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">\n' +
  '                    <tbody>\n' +
  '                      <tr>\n' +
  '                        <td align="left" class="editable-area" style="font-size:0px;padding:10px 25px;word-break:break-word;">\n' +
  '                          <div style="font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;text-align:left;color:#242424;">Kind regards,<br />Luci0\n' +
  '</div>\n' +
  '                        </td>\n' +
  '                      </tr>\n' +
  '                      <tr>\n' +
  '                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">\n' +
  '                          <div style="font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;text-align:left;color:#242424;">Journal name</div>\n' +
  '                        </td>\n' +
  '                      </tr>\n' +
  '                    </tbody>\n' +
  '                  </table>\n' +
  '                </div>\n' +
  '                <!--[if mso | IE]></td></tr></table><![endif]-->\n' +
  '              </div>\n' +
  '              <!--[if mso | IE]></td></tr></table><![endif]-->\n' +
  '            </td>\n' +
  '          </tr>\n' +
  '        </tbody>\n' +
  '      </table>\n' +
  '    </div>\n' +
  '    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\n' +
  '    <div style="margin:0px auto;max-width:600px;">\n' +
  '      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">\n' +
  '        <tbody>\n' +
  '          <tr>\n' +
  '            <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">\n' +
  '              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->\n' +
  '              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\n' +
  '                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">\n' +
  '                  <tbody>\n' +
  '                    <tr>\n' +
  '                      <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">\n' +
  '                        <div style="font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;font-size:11px;line-height:20px;text-align:left;color:#626262;">This email was sent to <a href="mailto:ioana.abacioaiei@hindawi.com" style="color: #15c; text-decoration: none">ioana.abacioaiei@hindawi.com</a>. You have received this email in regards to the account creation, submission, or peer review process of a submitted paper, published by GeoScienceWorld and supported by our publishing partner, Hindawi Limited. <br />\n' +
  '                          <br /> GeoScienceWorld, a nonprofit corporation conducting business in the Commonwealth of Virginia at 1750 Tysons Boulevard, Suite 1500, McLean, Virginia 22102. <br />\n' +
  '                          <br /> GeoScienceWorld respects your right to privacy. Please see our <a style="color: #007e92; text-decoration: none" href="https://www.hindawi.com/privacy/">privacy policy</a> for information on how we store, process, and safeguard your data. <p style="text-align: center; font-size: 12px">\n' +
  '                            <a href="http://localhost:3000/unsubscribe?id&#x3D;ad94204c-69ef-4bbc-9389-492a627b59bc&amp;token&#x3D;" style="color: #15c">Unsubscribe</a>\n' +
  '                          </p>\n' +
  '                        </div>\n' +
  '                      </td>\n' +
  '                    </tr>\n' +
  '                  </tbody>\n' +
  '                </table>\n' +
  '              </div>\n' +
  '              <!--[if mso | IE]></td></tr></table><![endif]-->\n' +
  '            </td>\n' +
  '          </tr>\n' +
  '        </tbody>\n' +
  '      </table>\n' +
  '    </div>\n' +
  '    <!--[if mso | IE]></td></tr></table><![endif]-->\n' +
  '  </div>\n' +
  '</body>\n' +
  '\n' +
  '</html>';

export function App() {
  const [state, setState] = useState({});
  return (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <Card>
        <MailTemplate
          html={html}
          onChange={(body, document) => {
            setState({ body, document });
          }}
        ></MailTemplate>
      </Card>
    </div>
  );
}

export default App;
