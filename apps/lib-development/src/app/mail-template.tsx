import React, { useState } from 'react';
import { MailTemplate, TemplateName } from '@hindawi/phenom-communications-ui';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { Select } from '@hindawi/phenom-ui';
const { Option } = Select;

const client = new ApolloClient({
  uri: 'http://localhost:3333/graphql',
  cache: new InMemoryCache(),
});

const signedTokens =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsaW5rcyI6eyJhZ3JlZUxpbmsiOiJodHRwOi8vbG9jYWxob3N0OjMwMDAvZW1haWxzL2FjY2VwdC1hY2FkZW1pYy1lZGl0b3I_dGVhbU1lbWJlcklkPWFkOTQyMDRjLTY5ZWYtNGJiYy05Mzg5LTQ5MmE2MjdiNTliYyZtYW51c2NyaXB0SWQ9YWQ5NDIwNGMtNjllZi00YmJjLTkzODktNDkyYTYyN2I1OWJjJnN1Ym1pc3Npb25JZD1lODI3ZjgxNy0zODg1LTRmMmItYTYzMy04NTk5Nzc0NjAwNTIiLCJkZWNsaW5lTGluayI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAwMC9lbWFpbHMvZGVjbGluZS1hY2FkZW1pYy1lZGl0b3I_dGVhbU1lbWJlcklkPWFkOTQyMDRjLTY5ZWYtNGJiYy05Mzg5LTQ5MmE2MjdiNTliYyIsInVuc3Vic2NyaWJlTGluayI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAwMC91bnN1YnNjcmliZT9pZD1hZDk0MjA0Yy02OWVmLTRiYmMtOTM4OS00OTJhNjI3YjU5YmMmdG9rZW49IiwibWFudXNjcmlwdERldGFpbHNMaW5rIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwL2RldGFpbHMvZTgyN2Y4MTctMzg4NS00ZjJiLWE2MzMtODU5OTc3NDYwMDUyLzI0YzA4YTQzLWVlYjQtNDkwNS1iZjUyLWJlM2I2MDlkOTFhYSIsInJldmlld0xpbmsiOiJodHRwOi8vbG9jYWxob3N0OjMwMDAifSwiZnJvbSI6eyJlbWFpbCI6Imx1Y2lhbi5oYXJoYXRhQGhpbmRhd2kuY29tIiwibmFtZSI6Ikx1Y2kwIn0sInRvIjp7ImVtYWlsIjoiaW9hbmEuYWJhY2lvYWllaUBoaW5kYXdpLmNvbSIsIm5hbWUiOiJMdWNpMCJ9LCJiY2MiOltdLCJwdWJsaXNoZXJOYW1lIjoiZ3N3IiwibWFudXNjcmlwdCI6eyJtYW51c2NyaXB0SWQiOiI2NjE5OTE5IiwiYWJzdHJhY3QiOiJTdW5zcG90cyBhcmUgZGFya2VyIGFyZWFzIG9uIHRoZSBTdW7igJlzIHBob3Rvc3BoZXJlIGFuZCBtb3N0IG9mIHNvbGFyIGVydXB0aW9ucyBvY2N1ciBpbiBjb21wbGV4IHN1bnNwb3QgZ3JvdXBzLiBUaGUgTW91bnQgV2lsc29uIGNsYXNzaWZpY2F0aW9uIHNjaGVtZSBkZXNjcmliZXMgdGhlIHNwYXRpYWwgZGlzdHJpYnV0aW9uIG9mIG1hZ25ldGljIHBvbGFyaXRpZXMgaW4gc3Vuc3BvdCBncm91cHMsIHdoaWNoIHBsYXlzIGFuIGltcG9ydGFudCByb2xlIGluIGZvcmVjYXN0aW5nIHNvbGFyIGZsYXJlcy4gV2l0aCB0aGUgcmFwaWQgYWNjdW11bGF0aW9uIG9mIHNvbGFyIG9ic2VydmF0aW9uIGRhdGEsIGF1dG9tYXRpYyByZWNvZ25pdGlvbiBvZiBtYWduZXRpYyB0eXBlIGluIHN1bnNwb3QgZ3JvdXBzIGlzIGltcGVyYXRpdmUgZm9yIHByb21wdCBzb2xhciBlcnVwdGlvbiBmb3JlY2FzdC4gV2UgcHJlc2VudCBpbiB0aGlzIHN0dWR5LCBiYXNlZCBvbiB0aGUgU0RPL0hNSSBTSEFSUCBkYXRhIHRha2VuIGR1cmluZyB0aGUgdGltZSBpbnRlcnZhbCAyMDEwLTIwMTcsIGFuIGF1dG9tYXRpYyBwcm9jZWR1cmUgZm9yIHRoZSByZWNvZ25pdGlvbiBvZiB0aGUgcHJlZGVmaW5lZCBtYWduZXRpYyB0eXBlcyBpbiBzdW5zcG90IGdyb3VwcyB1dGlsaXppbmcgYSBjb252b2x1dGlvbmFsIG5ldXJhbCBuZXR3b3JrIChDTk4pIG1ldGhvZC4gVGhyZWUgZGlmZmVyZW50IG1vZGVscyAoQSwgQiwgYW5kIEMpIHRha2UgbWFnbmV0b2dyYW1zLCBjb250aW51dW0gaW1hZ2VzLCBhbmQgdGhlIHR3by1jaGFubmVsIHBpY3R1cmVzIGFzIGlucHV0LCByZXNwZWN0aXZlbHkuIFRoZSByZXN1bHRzIHNob3cgdGhhdCBDTk4gaGFzIGEgcHJvZHVjdGl2ZSBwZXJmb3JtYW5jZSBpbiBpZGVudGlmaWNhdGlvbiBvZiB0aGUgbWFnbmV0aWMgdHlwZXMgaW4gc29sYXIgYWN0aXZlIHJlZ2lvbnMgKEFScykuIFRoZSBiZXN0IHJlY29nbml0aW9uIHJlc3VsdCBlbWVyZ2VzIHdoZW4gY29udGludXVtIGltYWdlcyBhcmUgdXNlZCBhcyBpbnB1dCBkYXRhIHNvbGVseSwgYW5kIHRoZSB0b3RhbCBhY2N1cmFjeSBleGNlZWRzIDk1JSwgZm9yIHdoaWNoIHRoZSByZWNvZ25pdGlvbiBhY2N1cmFjeSBvZiBBbHBoYSB0eXBlIHJlYWNoZXMgOTglIHdoaWxlIHRoZSBhY2N1cmFjeSBmb3IgQmV0YSB0eXBlIGlzIHNsaWdodGx5IGxvd2VyIGJ1dCBtYWludGFpbnMgYWJvdmUgODglLiIsImF1dGhvcnMiOlt7Im5hbWUiOiJTY3JlZW5pbmcyMSB0ZXN0aW5nIDIxIiwiYWZmaWxpYXRpb24iOiJ0ZXN0In0seyJuYW1lIjoiQXV0aG9yIDMgYXV0aG9yIDMiLCJhZmZpbGlhdGlvbiI6InRlc3QifV19LCJpYXQiOjE2MTU4ODQ3NTJ9.QyHBkaWCYda8qBpOf8BS6oVp1CI-d-lex4KprUYR_Lo';

const templateNames = Object.values(TemplateName);

export function App() {
  const [visible, setVisible] = useState(false);
  const [templateName, setTemplateName] = useState(templateNames[0]);
  return (
    <div style={{ padding: '20px' }}>
      <Select
        value={templateName}
        style={{ width: 250, marginBlockEnd: '20px' }}
        onChange={(val) => {
          setTemplateName(val);
        }}
      >
        {templateNames.map((option) => (
          <Option value={option} key={option}>
            {option}
          </Option>
        ))}
      </Select>

      <div style={{ width: '500px' }}>
        <MailTemplate
          visible={visible}
          mailMetadata={{
            templateName: templateName,
            mailInput: {
              manuscript: {
                manuscriptId: '3',
                manuscriptTitle: 'Manuscript title',
                journalName: 'Journal name',
                submittingAuthor: 'Jane Doe',
                comments: 'This is my comment',
              },
              logoUrl:
                'https://ci4.googleusercontent.com/proxy/l2B7lLP1vmAu0TBLXze-DpzWidHLv5sTh4ADY2dUieFUEi7IrqsXHtipuFv_ZYVIdIw=s0-d-e1-ft#https://i.imgur.com/dCbJ3C1.png',
            },
            signedInput: signedTokens,
          }}
          client={client}
          onClose={() => {
            setVisible(false);
          }}
          onBeforeSendMail={async () => {
            console.log('test before');
          }}
          onSendEmail={(data, e) => console.log(data, e)}
          onError={(err) => console.log(err)}
        />

        {/* <Button
        onClick={() => {
          setVisible(true);
        }}
      >
        Open Modal
      </Button> */}
        <button
          onClick={() => {
            setVisible(true);
          }}
        >
          Open Modal
        </button>
      </div>

      <div style={{ width: '500px' }}>
        <MailTemplate
          mailMetadata={{
            templateName: TemplateName.editorInvitationAccepted,
            mailInput: {
              manuscript: {
                manuscriptId: '22222',
                manuscriptTitle: 'Manuscript title',
                journalName: 'Journal name',
                submittingAuthor: 'Jane Doe',
                comments: 'This is my comment',
                authors: [
                  { name: 'Jane Abc', affiliation: 'Multiverse' },
                  { name: 'John Doe', affiliation: 'MyAffiliation' },
                  { name: 'Anne Doe', affiliation: 'MyAffiliation' },
                ],
                abstract: 'abstract',
              },
              logoUrl:
                'https://ci4.googleusercontent.com/proxy/l2B7lLP1vmAu0TBLXze-DpzWidHLv5sTh4ADY2dUieFUEi7IrqsXHtipuFv_ZYVIdIw=s0-d-e1-ft#https://i.imgur.com/dCbJ3C1.png',
            },
            signedInput: signedTokens,
          }}
          client={client}
          onBeforeSendMail={async () => {
            console.log('test before');
          }}
          onSendEmail={(data, e) => console.log(data, e)}
          onError={(err) => console.log(err)}
        >
          {(handleSendMail) => (
            <button
              onClick={() => {
                handleSendMail();
              }}
            >
              Send email directly
            </button>
          )}
        </MailTemplate>
      </div>
    </div>
  );
}

export default App;
