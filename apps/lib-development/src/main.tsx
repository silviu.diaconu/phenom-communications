import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter, Route, Switch } from 'react-router-dom';

import MailTemplateV2 from './app/mail-template-v2';
import MailTemplate from './app/mail-template';

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path='/v2'>
        <MailTemplateV2 />
      </Route>
      <Route path='/'>
        <MailTemplate />
      </Route>
    </Switch>
  </BrowserRouter>,
  document.getElementById('root'),
);
