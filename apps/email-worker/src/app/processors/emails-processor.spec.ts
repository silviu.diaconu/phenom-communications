import { Test } from '@nestjs/testing';
import { EmailsProcessor, SENDGRID_SERVICE } from './emails-processor';
import { EmailService } from '@hindawi/emails';
import { Job } from 'bull';

class MockSendgriService implements EmailService {
  send = jest.fn().mockResolvedValue({
    id: 'test',
    success: true
  });
}

describe('SendEmailService', () => {
  let emailsProcessor: EmailsProcessor;
  let mockSendgrid: EmailService;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        {
          provide: SENDGRID_SERVICE,
          useClass: MockSendgriService,
        },
        EmailsProcessor,
      ],
    }).compile();

    mockSendgrid = await moduleRef.resolve<EmailService>(SENDGRID_SERVICE);
    emailsProcessor = await moduleRef.resolve<EmailsProcessor>(EmailsProcessor);
  });

  describe('Get email message', () => {
    it('should process ', async () => {
      const input = { test: 'test' };

      await emailsProcessor.sendEmail({ data: input } as Job);

      expect(mockSendgrid.send).toHaveBeenCalledWith(input);
    });
  });
});
