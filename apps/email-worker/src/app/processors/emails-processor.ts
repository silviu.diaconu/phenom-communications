import { Inject, Logger } from '@nestjs/common';
import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import { EmailOptions, EmailResponse, EmailService } from '@hindawi/emails';
import { Queues, WorkerEvents } from '@hindawi/phenom-communications-types';

export const SENDGRID_SERVICE = 'SENDGRID_SERVICE';

@Processor(Queues.EMAILS_WORKER_QUEUE)
export class EmailsProcessor {
  private readonly logger = new Logger(EmailsProcessor.name);

  constructor(
    @Inject(SENDGRID_SERVICE)
    private readonly emailService: EmailService,
  ) {}

  @Process(WorkerEvents.SEND_EMAIL)
  async sendEmail(job: Job<EmailOptions>): Promise<EmailResponse> {
    this.logger.debug('Started job ' + job.id + ' Send email ...');

    const options: EmailOptions = job.data;

    const emailResponse = await this.emailService.send(options);

    if (!emailResponse.success) {
      throw Error(
        'Failed sending email: ' + emailResponse.statusCode + '. ' + emailResponse.messages,
      );
    }
    return emailResponse;
  }
}
