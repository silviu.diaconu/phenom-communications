import { Module } from '@nestjs/common';

import { BullModule } from '@nestjs/bull';
import { EmailsProcessor, SENDGRID_SERVICE } from './processors/emails-processor';
import { EmailsFactory } from '@hindawi/emails';
import { Queues } from '@hindawi/phenom-communications-types';
import { RedisConfig } from './config/redis.config';
import { ConfigsModule } from './config/configs-module';
import { SendgridConfig } from './config/sendgrid-config';

@Module({
  imports: [
    ConfigsModule,
    BullModule.forRootAsync({
      imports: [ConfigsModule],
      useFactory: (redisConfig: RedisConfig) => {
        return redisConfig.queueOptions();
      },
      inject: [RedisConfig],
    }),
    BullModule.registerQueue({
      name: Queues.EMAILS_WORKER_QUEUE,
    }),
  ],
  providers: [
    EmailsProcessor,
    {
      provide: SENDGRID_SERVICE,
      useFactory: (config: SendgridConfig) => {
        return EmailsFactory.sendgrid(config.key(), false);
      },
      inject: [SendgridConfig],
    },
  ],
})
export class AppModule {}
