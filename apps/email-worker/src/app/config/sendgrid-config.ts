import { Injectable } from '@nestjs/common';

@Injectable()
export class SendgridConfig {
  key() {
    return process.env.SENDGRID_API_SECRET;
  }
}
