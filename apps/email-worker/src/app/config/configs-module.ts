import { Module } from '@nestjs/common';
import { RedisConfig } from './redis.config';
import { ConfigModule } from '@nestjs/config';
import { SendgridConfig } from './sendgrid-config';

const providers = [RedisConfig, SendgridConfig];

@Module({
  imports: [ConfigModule.forRoot()],
  providers: providers,
  exports: providers,
})
export class ConfigsModule {}
