import { Injectable } from '@nestjs/common';
import { QueueOptions } from 'bull';

@Injectable()
export class RedisConfig {
  queueOptions(): QueueOptions {
    return {
      redis: {
        host: process.env.REDIS_HOST,
        port: parseInt(process.env.REDIS_PORT),
      },
    };
  }
}
