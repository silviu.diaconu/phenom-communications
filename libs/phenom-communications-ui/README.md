# phenom-communications-ui

This library has the core ui component that interacts with the phenom-communications-backend

# Installing

To install the component simply use

```npm
npm i @hindawi/phenom-communications-ui
```

# Using the component

## Import dependencies

```typescript
import { MailTemplate, TemplateName } from '@hindawi/phenom-communications-ui';
import { ApolloClient, InMemoryCache } from '@apollo/client';
```

## Import CSS

```typescript
import '@hindawi/phenom-communications-ui/phenom-communications-ui.umd.css';
```

## Instantiate a new apollo client

Old versions of apollo/client also work

```typescript
const client = new ApolloClient({
  uri: 'http://localhost:4200/graphql',
  cache: new InMemoryCache(),
});
```

## Pass the props to the component

```typescript
<MailTemplate
  visible={true}
  mailMetadata={{
    templateName: TemplateName.invitationToHandleManuscript,
    mailInput: {
      to: {
        email: 'receiver@hindawi.com',
        name: 'Author',
      },
      from: {
        email: 'sender@hindawi.com',
        name: 'Triage Editor',
      },
      manuscript: {
        manuscriptId: '22222',
        manuscriptTitle: 'Manuscript title',
        journalName: 'Journal name',
        submittingAuthor: 'Jane Doe',
        authors: [
          { name: 'Jane Abc', affiliation: 'Multiverse' },
          { name: 'John Doe', affiliation: 'MyAffiliation' },
          { name: 'Anne Doe', affiliation: 'MyAffiliation' },
        ],
        abstract: 'abstract',
      },
    },
  }}
  client={client}
  onClose={() => {}}
  onBeforeSendMail={async () => {}}
  onSendEmail={(data, e) => {}}
  onError={(err) => {}}
/>
```

# Using render props

Using render props will not display the component, but will provide access to the sendMail function.

```typescript
<MailTemplate
  mailMetadata={{
    templateName: TemplateName.editorInvitationAccepted,
    mailInput: {
      to: {
        email: 'receiver@hindawi.com',
        name: 'Receiver',
      },
      from: {
        email: 'sender@hindawi.com',
        name: 'Sender Editor',
      },
      manuscript: {
        manuscriptId: '22222',
        manuscriptTitle: 'Manuscript title',
        journalName: 'Journal name',
        submittingAuthor: 'Jane Doe',
        comments: 'This is my comment',
        authors: [
          { name: 'Jane Abc', affiliation: 'Multiverse' },
          { name: 'John Doe', affiliation: 'MyAffiliation' },
          { name: 'Anne Doe', affiliation: 'MyAffiliation' },
        ],
        abstract: 'abstract',
      },
    },
  }}
  client={client}
  onBeforeSendMail={async () => {
    console.log('test before');
  }}
  onSendEmail={(data, e) => console.log(data, e)}
  onError={(err) => console.log(err)}
>
  {(handleSendMail) => (
    <button
      onClick={() => {
        handleSendMail();
      }}
    >
      Send email directly
    </button>
  )}
</MailTemplate>
```

# Props

| Prop              | Type                                                                                |
| ----------------- | ----------------------------------------------------------------------------------- |
| visible?          | `boolean`                                                                           |
| mailMetadata      | `TemplateInputDto`                                                                  |
| client            | `any apollo client `                                                                |
| onClose?          | `(e: React.MouseEvent<HTMLElement, MouseEvent>) => void`                            |
| onBeforeSendMail? | `(e: React.MouseEvent<HTMLElement, MouseEvent>) => Promise<void>`                   |
| onSendMail?       | `(response: any) => void`                                                           |
| onSendEmail?      | `(data: SendMailResponseDto, e: React.MouseEvent<HTMLElement, MouseEvent>) => void` |
