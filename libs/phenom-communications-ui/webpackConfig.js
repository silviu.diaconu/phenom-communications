import TerserPlugin from 'terser-webpack-plugin';

export default (config) => {
  return {
    ...config,
    externals: [],
    target: 'web',
    plugins: [
      ...config.plugins,
      new TerserPlugin({
        parallel: true,
        terserOptions: {
          ecma: 6,
        },
      }),
    ],
    devtool: false,
    output: {
      ...config.output,
      filename: 'bundle.js',
      libraryTarget: 'commonjs',
    },
  };
};
