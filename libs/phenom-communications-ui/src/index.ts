import { MailTemplate } from './lib/mail-template/MailTemplate';
import {
  TemplateInputDto,
  SendMailResponseDto,
  TemplateName,
} from '@hindawi/phenom-communications-types';
export { MailTemplate, TemplateInputDto, TemplateName, SendMailResponseDto };
