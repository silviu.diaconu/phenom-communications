import React from 'react';
import { replaceHtmlMailBody } from '../utils/replaceHtmlBody';
import { Editor } from './Editor';
import { Row, Spinner } from '@hindawi/phenom-ui';

interface MailBodyProps {
  html: string;
  editableAreas: string[];
  onEditorChange: (content: string, index: number) => void;
}

export const MailBody: React.FunctionComponent<MailBodyProps> = ({
  html,
  editableAreas,
  onEditorChange,
}) => {
  const editors = editableAreas.map((content, index) => {
    return <Editor mailBody={content} onEditorChange={(v: string) => onEditorChange(v, index)} />;
  });

  return editableAreas.length > 0 ? (
    replaceHtmlMailBody(html, editors)
  ) : (
    <Row style={{ height: '100%' }} align='middle'>
      <Spinner size='large' />;
    </Row>
  );
};

export const MemoizedMailBody = React.memo(MailBody);
