import React, { Fragment } from 'react';
import { Button } from '@hindawi/phenom-ui';

interface ModalFooterProps {
  onRevert: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  onSendEmail: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
}

export const ModalFooter: React.FunctionComponent<ModalFooterProps> = ({
  onRevert,
  onSendEmail,
}) => {
  return (
    <Fragment>
      <Button onClick={onRevert}>REVERT CHANGES</Button>
      <Button type='primary' onClick={onSendEmail}>
        SEND EMAIL
      </Button>
    </Fragment>
  );
};
