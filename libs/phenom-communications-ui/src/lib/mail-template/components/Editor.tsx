import React, { useEffect, useRef, useState } from 'react';
import ReactQuill from 'react-quill';
import { IconEdit } from '@hindawi/phenom-ui';
import { toolbarOptions } from '../config';
import { useOnClickOutside } from '../utils';
import { Sources } from 'quill';

enum EditorCSSClasses {
  editable = 'quill-wrapper',
  notEditable = 'quill-wrapper is-not-editable',
}

function renderEditorCss(isEditable: boolean): EditorCSSClasses {
  return !isEditable ? EditorCSSClasses.notEditable : EditorCSSClasses.editable;
}

export interface EditorProps {
  mailBody: string;
  onEditorChange: Function;
}

export const Editor: React.FunctionComponent<EditorProps> = ({ mailBody, onEditorChange }) => {
  const [isEditable, setIsEditable] = useState(false);
  const containerRef = useRef();
  const quillRef = useRef();

  useEffect(() => {
    if (isEditable) {
      quillRef.current.editor.setSelection(mailBody.length, 0);
    }
  }, [isEditable]);

  useOnClickOutside(containerRef, () => {
    setIsEditable(false);
  });

  return (
    <div
      ref={containerRef}
      className={renderEditorCss(isEditable)}
      onClick={() => {
        setIsEditable(true);
      }}
    >
      <IconEdit className='edit-icon'></IconEdit>
      <ReactQuill
        ref={quillRef}
        modules={{ toolbar: toolbarOptions }}
        value={mailBody}
        onChange={(value: string, _, source: Sources) => {
          if (source === 'user') {
            onEditorChange(value);
          }
        }}
      />
    </div>
  );
};
