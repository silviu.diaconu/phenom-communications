import React from 'react';
import { MailBody, ModalFooter } from './components';

import './assets/main.scss';
import { StyledCard, StyledCol, StyledMailBody, StyledModal } from './MailTemplate.styles';

import { useMailTemplate } from './useMailTemplate';
import { TemplateInputDto, SendMailResponseDto } from '@hindawi/phenom-communications-types';
import { Row } from '@hindawi/phenom-ui';

interface MailTemplateProps {
  visible?: boolean;
  mailMetadata: TemplateInputDto;
  client: any;
  onClose?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  onBeforeSendMail?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => Promise<void>;
  onError?: (response: any) => void;
  onSendEmail?: (data: SendMailResponseDto, e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  onSendEmailOverride?: (
    cb: (templateInput: TemplateInputDto, e?: React.MouseEvent<HTMLElement, MouseEvent>) => void,
    staleMailMetadata: TemplateInputDto,
  ) => void;
}

export const MailTemplate: React.FC<MailTemplateProps> = ({
  visible = true,
  mailMetadata,
  client,
  children,
  onClose,
  onBeforeSendMail,
  onSendEmail,
  onError,
  onSendEmailOverride,
}) => {
  const {
    html,
    mailBody,
    currentMailBody,
    setCurrentMailBody,
    handleMailSubmission,
    handleOnRevert,
    handleOnClose,
  } = useMailTemplate({
    visible,
    client,
    onBeforeSendMail,
    onError,
    onSendEmail,
    onSendEmailOverride,
    onClose,
    mailMetadata,
  });

  return typeof children === 'function' ? (
    children(handleMailSubmission)
  ) : (
    <StyledModal
      bodyStyle={{ height: '70vh' }}
      visible={visible}
      title='Email editor'
      width='80%'
      footer={<ModalFooter onRevert={handleOnRevert} onSendEmail={handleMailSubmission} />}
      onCancel={handleOnClose}
    >
      <StyledCard bodyStyle={{ paddingInlineEnd: '0', paddingInlineStart: '0' }}>
        <Row>
          <StyledCol span={24}>
            {mailBody && (
              <StyledMailBody>
                <MailBody
                  html={html}
                  editableAreas={currentMailBody}
                  onEditorChange={setCurrentMailBody}
                />
              </StyledMailBody>
            )}
          </StyledCol>
        </Row>
      </StyledCard>
    </StyledModal>
  );
};
