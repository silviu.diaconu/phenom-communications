import { useEffect, useState } from 'react';
import { SendMailResponseDto, TemplateInputDto } from '@hindawi/phenom-communications-types';
import { sendMailMutation, templateQuery } from './graphql/queries';

interface UseMailTemplateProps {
  visible?: boolean;
  mailMetadata: TemplateInputDto;
  // apollo client, is compatible with react-appollo and @apollo/client
  client: any;
  onClose?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  onBeforeSendMail?: (e: React.MouseEvent<HTMLElement, MouseEvent>) => Promise<void>;
  onError?: (response: any) => void;
  onSendEmail?: (data: SendMailResponseDto, e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  onSendEmailOverride?: (
    cb: (templateInput: TemplateInputDto, e?: React.MouseEvent<HTMLElement, MouseEvent>) => void,
    staleMailMetadata: TemplateInputDto,
  ) => void;
}

interface UseMailTemplateReturn {
  html: string;
  setHtml: React.Dispatch<React.SetStateAction<string>>;
  mailBody: string[];
  setMailBody: React.Dispatch<React.SetStateAction<string[]>>;
  currentMailBody: string[];
  setCurrentMailBody: (content: string, index: number) => void;
  handleMailSubmission: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  handleOnRevert: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  handleOnClose: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
}

type UseMailTemplateHook = (props: UseMailTemplateProps) => UseMailTemplateReturn;

export const useMailTemplate: UseMailTemplateHook = ({
  visible,
  mailMetadata,
  client,
  onBeforeSendMail,
  onError,
  onSendEmail,
  onClose,
  onSendEmailOverride,
}) => {
  const [html, setHtml] = useState<string>('');
  const [mailBody, setMailBody] = useState<string[]>([]);
  const [currentMailBody, setCurrMailBody] = useState<string[]>([]);

  const setCurrentMailBody = (content: string, index: number): void => {
    const current = [...currentMailBody];
    current[index] = content;
    setCurrMailBody(current);
  };

  useEffect(() => {
    const getMessage = async () => {
      const { data } = await getTemplate(mailMetadata);
      const { html, body } = data.template;

      setHtml(html);
      setMailBody([...body]);
      setCurrMailBody([...body]);
    };

    if (visible) {
      getMessage();
    }
  }, [visible]);

  const handleSendMail = async (
    templateInput: TemplateInputDto,
    e?: React.MouseEvent<HTMLElement, MouseEvent>,
  ) => {
    const response = await sendTemplate(templateInput);
    if (typeof onSendEmail === 'function') {
      onSendEmail(response, e);
    }
  };

  const handleMailSubmission = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const { mailInput } = mailMetadata;
    const templateInput = {
      ...mailMetadata,
      mailInput: { ...mailInput, body: currentMailBody },
    };

    if (typeof onSendEmailOverride === 'function') {
      try {
        (async () => {
          await onBeforeSendMail(e);
          await onSendEmailOverride(handleSendMail, templateInput);
        })();
      } catch (err) {
        onError(err);
      }
      return;
    } else {
      (async () => {
        try {
          if (typeof onBeforeSendMail === 'function') {
            await onBeforeSendMail(e);
            await handleSendMail(templateInput, e);
            return;
          }

          await handleSendMail(templateInput, e);
        } catch (err) {
          onError(err);
        }
      })();
    }
  };

  const sendTemplate = (input: TemplateInputDto) => {
    return client.mutate({
      mutation: sendMailMutation,
      variables: {
        templateInput: {
          ...input,
        },
      },
    });
  };

  const getTemplate = (input: TemplateInputDto) => {
    return client.query({
      query: templateQuery,
      variables: {
        templateInput: {
          ...input,
        },
      },
    });
  };

  const handleOnRevert = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    setCurrMailBody(mailBody);
  };

  const handleOnClose = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    onClose(e);
  };

  return {
    html,
    setHtml,
    mailBody,
    setMailBody,
    currentMailBody,
    setCurrentMailBody,
    handleMailSubmission,
    handleOnRevert,
    handleOnClose,
  };
};
