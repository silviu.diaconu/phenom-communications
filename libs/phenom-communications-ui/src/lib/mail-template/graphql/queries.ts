import gql from 'graphql-tag';

export const templateQuery = gql`
  query template($templateInput: TemplateInput!) {
    template(templateInput: $templateInput) {
      html
      body
    }
  }
`;

export const sendMailMutation = gql`
  mutation sendEmail($templateInput: TemplateInput!) {
    sendEmail(templateInput: $templateInput) {
      success
    }
  }
`;
