import { Card, Col, Modal } from '@hindawi/phenom-ui';
import styled from 'styled-components';

export const StyledModal = styled(Modal)`
  max-width: 1280px;
`;

export const StyledCard = styled(Card)`
  max-width: 100%;
  height: 100%;
  overflow-y: scroll;
`;

export const StyledCol = styled(Col)`
  display: flex;
  justify-content: center;
  height: 640px;
`;

export const StyledMailBody = styled.div`
  a {
    pointer-events: none;
  }
  max-width: 550px;
`;
