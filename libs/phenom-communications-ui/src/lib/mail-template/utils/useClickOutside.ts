import { useEffect } from 'react';

type EventListenerFn = (this: Document, event: MouseEvent) => void;

export function useOnClickOutside(
  ref: React.MutableRefObject<any>,
  handler: (e: MouseEvent) => void,
) {
  useEffect(() => {
    const listener: EventListenerFn = (event) => {
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }

      handler(event);
    };

    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);

    return () => {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, handler]);
}
