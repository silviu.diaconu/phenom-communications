import { gql, ApolloQueryResult, FetchResult } from '@apollo/client';
import { client } from '../config';
import {
  EmailResponse,
  EmailTemplateDTO,
  EmailTemplateTokensInput,
} from '@hindawi/phenom-communications-types';

export interface MutationResponse {
  sendEmail: EmailResponse;
}
export interface QueryResponse {
  getMessage: EmailTemplateDTO;
}

export type EmailTemplateInput = {
  templateName: string;
  emailDetails: EmailTemplateTokensInput;
};

export class GraphQLService {
  getMessage(input: EmailTemplateInput): Promise<ApolloQueryResult<QueryResponse>> {
    const {
      emailDetails: { to, from, manuscript },
      templateName,
    } = input;
    return client.query<QueryResponse>({
      query: gql`
        query getMessage($emailDetails: EmailTemplateTokensInput!, $templateName: String!) {
          getMessage(emailDetails: $emailDetails, templateName: $templateName) {
            html
            body {
              body1
              body2
              body3
              body4
              body5
            }
          }
        }
      `,
      variables: {
        emailDetails: {
          to,
          from,
          manuscript,
        },
        templateName,
      },
    });
  }

  sendTemplate(input: EmailTemplateInput): Promise<FetchResult<MutationResponse>> {
    const {
      emailDetails: { to, from, manuscript, body },
      templateName,
    } = input;
    return client.mutate<MutationResponse>({
      mutation: gql`
        mutation sendEmail($emailDetails: EmailTemplateTokensInput!, $templateName: String!) {
          sendEmail(emailDetails: $emailDetails, templateName: $templateName) {
            success
            messages
          }
        }
      `,
      variables: {
        emailDetails: {
          to,
          from,
          body,
          manuscript,
        },
        templateName,
      },
    });
  }
}

// export const GqlService = new GraphQLService();
