import { TemplateName } from './TemplateName.enum';

export enum PublisherName {
  gsw = 'gsw',
  hindawi = 'hindawi',
}
export interface ContactDto {
  email: string;
  name: string;
}

export class AuthorDto {
  name: string;
  affiliation: string;
}

export interface ManuscriptDto {
  manuscriptId?: string;
  manuscriptTitle?: string;
  authors?: AuthorDto[];
  journalName?: string;
  abstract?: string;
  submittingAuthor?: string;
  comments?: string;
}

export interface LinksDto {
  agreeLink?: string;
  declineLink?: string;
  manuscriptDetailsLink?: string;
  reviewLink?: string;
  unsubscribeLink?: string;
}

export interface MailInputDto {
  to?: ContactDto;
  from?: ContactDto;
  bcc?: ContactDto[];
  replyTo?: ContactDto;
  manuscript?: ManuscriptDto;
  links?: LinksDto;
  body?: string[];
  logoUrl?: string;
  publisherName?: PublisherName;
}

export interface EditableInputDto {
  logoUrl: string;
  manuscript?: ManuscriptDto;
  body?: string[];
}

export interface SignedInputDto {
  to?: ContactDto;
  from?: ContactDto;
  bcc?: ContactDto[];
  manuscript?: ManuscriptDto;
  links?: LinksDto;
  body?: string[];
  publisherName?: PublisherName;
}

export interface TemplateInputDto {
  templateName: TemplateName;
  mailInput: MailInputDto;
  signedInput: string;
}

export interface SendMailResponseDto {
  success: boolean;
  statusCode: string;
  messages?: string[];
}
