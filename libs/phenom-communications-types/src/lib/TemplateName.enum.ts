export enum TemplateName {
  revisionRequested = 'revisionRequested',
  invitationToHandleManuscript = 'invitationToHandleManuscript',
  manuscriptRejected = 'manuscriptRejected',
  recommendationToPublish = 'recommendationToPublish',
  recommendationToReject = 'recommendationToReject',
  editorInvitationAccepted = 'editorInvitationAccepted',
  editorDeclined = 'editorDeclined',
  editorialDecisionReturnedWithComments = 'editorialDecisionReturnedWithComments',
  rejectWithPeerReviews = 'rejectWithPeerReviews',
  rejectNoPeerReviews = 'rejectNoPeerReviews',
  recommendationToRevise = 'recommendationToRevise',
}
