export enum Queues {
  EMAILS_WORKER_QUEUE = 'EMAILS_WORKER_QUEUE',
}

export enum WorkerEvents {
  SEND_EMAIL = 'SEND_EMAIL',
}
