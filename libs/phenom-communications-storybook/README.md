# phenom-communications-storybook

This library was generated with [Nx](https://nx.dev).

## Running storybook

Run `nx serve phenom-communications-storybook:storybook` to execute the storybook for all applications.

Run `nx serve APP_NAME:storybook` to execute the storybook for a specific application.
