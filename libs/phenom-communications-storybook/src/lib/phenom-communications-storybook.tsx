import React from 'react';

import './phenom-communications-storybook.module.scss';

/* eslint-disable-next-line */
export interface PhenomCommunicationsStorybookProps {}

export function PhenomCommunicationsStorybook(props: PhenomCommunicationsStorybookProps) {
  return (
    <div>
      <h1>Welcome to phenom-communications-storybook!</h1>
    </div>
  );
}

export default PhenomCommunicationsStorybook;
