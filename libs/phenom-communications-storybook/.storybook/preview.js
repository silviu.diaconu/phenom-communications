import { addDecorator, configure } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';

addDecorator(withKnobs);
configure(require.context('../../phenom-mail-template', true, /\.stories\.tsx?$/), module);
