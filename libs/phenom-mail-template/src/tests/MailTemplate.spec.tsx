import React from 'react';
import '@testing-library/jest-dom';
import { fireEvent, render } from '@testing-library/react';
import { MailTemplate } from '../lib/MailTemplate';
import { html as edtiableAreasHtml } from './fixtures/editableAreasTemplate';
import { html } from './fixtures/noEditableAreasTemplate';

describe('MailTemplate component', () => {
  it('should render the html', () => {
    const { getByText } = render(<MailTemplate html={edtiableAreasHtml} onChange={() => {}} />);
    expect(getByText('Dear Luci0,')).toBeVisible();
  });

  it('should render the html without instaces of quill editor if edtiable-area class is missing', () => {
    const { getByText, container } = render(<MailTemplate html={html} onChange={() => {}} />);
    const editors = container.querySelectorAll('.ql-editor');

    expect(getByText('Dear Luci0,')).toBeVisible();
    expect(editors.length).toBe(0);
  });

  it('should replace the innerHTML of editable-area elements with instances of quill', () => {
    const { container } = render(<MailTemplate html={edtiableAreasHtml} onChange={() => {}} />);
    const editors = container.querySelectorAll('.ql-editor');

    expect(editors.length).toBe(2);
  });

  it('focusing a editable area should make the overlay dissapear', () => {
    const { container } = render(<MailTemplate html={edtiableAreasHtml} onChange={() => {}} />);
    const quillWrapper = container.querySelectorAll('.quill-wrapper-container')[0];
    const editor = container.querySelectorAll('.ql-editor')[0];

    expect(quillWrapper.classList).toContain('is-not-editable');

    fireEvent.click(editor);

    expect(quillWrapper.classList).not.toContain('is-not-editable');
  });
});
