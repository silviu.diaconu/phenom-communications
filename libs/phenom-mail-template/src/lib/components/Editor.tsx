import React, { useEffect, useRef, useState } from 'react';
import ReactQuill from 'react-quill';
import { IconEdit } from '@hindawi/phenom-ui';
import { toolbarOptions } from '../config';
import { useOnClickOutside } from '../utils';
import { Sources } from 'quill';

enum EditorCSSClasses {
  editable = 'quill-wrapper-container',
  notEditable = 'quill-wrapper-container is-not-editable',
}

function renderEditorCss(isEditable: boolean): EditorCSSClasses {
  return !isEditable ? EditorCSSClasses.notEditable : EditorCSSClasses.editable;
}

export interface EditorProps {
  mailBody: string;
  onEditorChange: Function;
}

export const Editor: React.FunctionComponent<EditorProps> = ({ mailBody, onEditorChange }) => {
  const [isEditable, setIsEditable] = useState(false);
  const containerRef = useRef(null);
  const quillRef = useRef(null);

  useOnClickOutside(containerRef, () => {
    setIsEditable(false);
  });

  return (
    <div
      ref={containerRef}
      className={renderEditorCss(isEditable)}
      onClick={() => {
        setIsEditable(true);
      }}
    >
      <IconEdit className='edit-icon'></IconEdit>
      <ReactQuill
        ref={quillRef}
        modules={{ toolbar: toolbarOptions }}
        value={mailBody}
        onChange={(value: string, _, source: Sources) => {
          if (source === 'user') {
            onEditorChange(value);
          }
        }}
      />
    </div>
  );
};
