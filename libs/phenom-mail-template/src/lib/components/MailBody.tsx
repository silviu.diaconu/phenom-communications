import React from 'react';
import { replaceHtmlMailBody } from '../utils/replaceHtmlBody';
import { Editor } from './Editor';

interface MailBodyProps {
  html: string;
  editableAreas: string[];
  onEditorChange: (content: string, index: number) => void;
}

export const MailBody: React.FunctionComponent<MailBodyProps> = ({
  html,
  editableAreas,
  onEditorChange,
}) => {
  const editors = editableAreas.map((content, index) => {
    return <Editor mailBody={content} onEditorChange={(v: string) => onEditorChange(v, index)} />;
  });

  return replaceHtmlMailBody(html, editors);
};

export const MemoizedMailBody = React.memo(MailBody);
