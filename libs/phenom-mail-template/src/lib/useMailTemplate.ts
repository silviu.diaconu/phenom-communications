import { useEffect, useRef, useState } from 'react';

interface UseMailTemplateProps {
  html: string;
  onChange: (values: string[], document: string) => void;
}

interface UseMailTemplateReturn {
  editorValues: string[];
  setCurrentEditorValue: (content: string, index: number) => void;
}

type UseMailTemplateHook = (props: UseMailTemplateProps) => UseMailTemplateReturn;

const getEditableAreas = (html: string): string[] => {
  const dom = new DOMParser().parseFromString(html, 'text/html');
  const areas: string[] = [];
  dom.querySelectorAll('.editable-area').forEach((el) => areas.push(el.innerHTML));
  return areas;
};

const getEditableNodes = (dom: Document): Record<number, Element> => {
  const editableNodes: Record<number, Element> = {};
  dom.querySelectorAll('.editable-area').forEach((el, idx) => (editableNodes[idx] = el));

  return editableNodes;
};

export const useMailTemplate: UseMailTemplateHook = ({ html, onChange }) => {
  const dom = useRef(new DOMParser().parseFromString(html, 'text/html'));
  const updatedDomNodes = useRef(getEditableNodes(dom.current));
  const [editorValues, setEditorValues] = useState<string[]>([]);

  const setCurrentEditorValue = (content: string, index: number): void => {
    const current = [...editorValues];
    current[index] = content;

    updatedDomNodes.current[index].innerHTML = content;

    const updatedHtml = new XMLSerializer().serializeToString(dom.current);
    setEditorValues(current);
    onChange(current, updatedHtml);
  };

  useEffect(() => {
    const areas = getEditableAreas(html);
    setEditorValues([...areas]);
  }, []);

  return {
    editorValues,
    setCurrentEditorValue,
  };
};
