import React from 'react';
import parse, { Element } from 'html-react-parser';
import { empty } from '../components/Empty';
import { Editor } from '../components/Editor';

export function replaceHtmlMailBody(html: string, containers: JSX.Element[]): JSX.Element {
  let count = 0;
  let styleCount = 0;

  return parse(html, {
    replace: (domNode: Element) => {
      if (domNode.tagName === 'body') {
        domNode.attribs.style += 'height:100%';
        return domNode;
      }
      if (domNode.tagName === 'style') {
        if (styleCount === 0) {
          styleCount++;
          return empty;
        }
      }

      if (domNode.attribs?.class?.includes('editable-area')) {
        const elem = containers[count];
        count++;
        return elem;
      }
    },
  }) as JSX.Element;
}
