import styled from 'styled-components';

export const StyledMailBody = styled.div`
  a {
    pointer-events: none;
  }
  max-width: 550px;
`;
