import React from 'react';
import { MailBody } from './components';

import { useMailTemplate } from './useMailTemplate';
import './assets/main.scss';
import { StyledMailBody } from './MailTemplate.styles';

interface MailTemplateProps {
  html: string;
  onChange: (values: string[], document: string) => void;
}

export const MailTemplate: React.FC<MailTemplateProps> = ({ html, onChange = () => {} }) => {
  const { editorValues, setCurrentEditorValue } = useMailTemplate({
    html,
    onChange,
  });

  return (
    <StyledMailBody>
      <MailBody html={html} editableAreas={editorValues} onEditorChange={setCurrentEditorValue} />
    </StyledMailBody>
  );
};
